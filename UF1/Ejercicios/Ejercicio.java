package Ejercicios;

import java.util.ArrayList;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Pais{
    String pais;
    int votos;
}

public class Ejercicio {
    public static void main(String[] args) {
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            ArrayList<Pais> paises = new ArrayList<Pais>();
            int votos = 1;
            while(votos!=0){
                votos = Integer.parseInt(buffer.readLine());
                String votacion = buffer.readLine();
                String[] votacion_convertida = votacion.split(" ");
                System.out.println(Arrays.toString(votacion_convertida));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
