import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arturkravchukex3 {
    public static void main(String[] args){
        System.out.println("PIDE NÚMEROS");
        System.out.print("Escribe un número entero positivo: ");

        // Inicia un try junto al BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Guarda un valor int en n1 escrito por teclado.
            int n1 = Integer.parseInt(buffer.readLine());

            // Comprueba si es positivo.
            if(0<n1){
                System.out.print("Escriba un múltiplo de "+n1+": ");
                // Guarda un valor int en n2 escrito por teclado.
                int n2 = Integer.parseInt(buffer.readLine());
                // Comprueba si n2 es mayor a n1.
                if(n1<n2){
                    // Comprueba si n2 es multiplo de n1.
                    if(n2%n1==0){
                        System.out.print("Escriba un divisor de "+n2+" distinto de "+n1+": ");
                        // Guarda un valor int en n2 escrito por teclado.
                        int n3 = Integer.parseInt(buffer.readLine());
                        // Comprueba si n3 no es igual a n1.
                        if(n3!=n1){
                            // Comprueba si n2 es mayor a n3.
                            if(n3<n2){
                                // Comprueba si n3 es divisor de n2.
                                if(n2%n3==0){
                                    System.out.println("¡Gracias por su colaboración!");
                                // Si n3 no es divisor de n2, mostrarlo por pantalla y salir del if.
                                }else{
                                    System.out.println("¡"+n3+" no es divisor de "+n2+"!");
                                }
                            // Si n2 no es mayor a n3, mostrarlo por pantalla y salir del if.
                            }else{
                                System.out.println("¡El divisor debe ser menor que "+n2+"!");
                            }
                        // Si n3 es igual a n1, mostrarlo por pantalla y salir del if.
                        }else{
                            System.out.println("¡Le he pedido un número distinto de "+n1+"!");
                        }
                    // Si n2 no es multiplo de n1, mostrarlo por pantalla y salir del if.
                    }else{
                        System.out.println("¡"+n2+" no es multiplo de "+n1+"!");
                    }
                // Si n2 no es mayor a n1, mostrarlo por pantalla  y salir del if.
                }else{
                    System.out.println("¡El multiplo debe ser mayor que "+n1+"!");
                }
            // Si n1 no es positivo, mostrarlo por pantalla y salir del if.
            }else{
                System.out.println("¡No ha escrito un número entero positivo!");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }

    }
}
