package act_repaso;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

public class Akravchukptrepasoej3 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            Random random = new Random();
            int numero_multis = Integer.parseInt(buffer.readLine());
            int[] notas = new int[numero_multis];
            int notafinal = 0;
            if(0<numero_multis){
                for(int i = 0; i<numero_multis; i++){
                    int a = random.nextInt(9)+2;
                    int b = random.nextInt(9)+2;
                    System.out.println("¿Cuanto es "+a+" x "+b+"?");
                    int res = (Integer.parseInt(buffer.readLine())==a*b)?1:0;
                    notas[i] = (res==1)?10:0;
                    System.out.println((res==1)?"¡RESPUESTA CORRECTA!":"¡RESPUESTA INCORRECTA!");
                }
                for(int i = 0; i<numero_multis; i++){
                    notafinal=notafinal+notas[i];
                }
                notafinal= notafinal/numero_multis;
                System.out.println("Le corresponde una nota de "+notafinal);
            }else{
                System.out.println("¡El numero de preguntas debe ser al menos 1!");
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
