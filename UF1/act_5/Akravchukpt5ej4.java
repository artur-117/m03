package act_5;
import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;



public class Akravchukpt5ej4 {
    public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) {
        try{
            // 
            System.out.print("Introduce la contraseña: ");
            // Inicia booleanos en falso, debera ir superando estos booleanos para cumplir los requisitos de la contraseña.
            Boolean my = false;
            Boolean mn = false;
            Boolean num = false;
            Boolean naf = false;
            // Te permite escribir la contraseña en oculto.
            Console console = System.console();
            char[] passwordChars = console.readPassword();
            String passwd = new String(passwordChars);
            // Establece minimo 8 caracteres.
            if(8<=passwd.length()){
                // Recorre cada caracter de la contraseña comprobando si cumple los requisitos de caracteres, va cambiando los booleanos a true o false dependiendo si cumple o no.
                for(int i = 0; i<passwd.length();i++){
                    if(passwd.charAt(i)>='A' && passwd.charAt(i)<='Z'){
                        my = true;
                    }else if(passwd.charAt(i)>='a' && passwd.charAt(i)<='z'){
                        mn = true;
                    }else if(passwd.charAt(i) >= '0' & passwd.charAt(i)<='9'){
                        num = true;
                    }else if(passwd.charAt(i)==' '){
                        my = false;
                        break;
                    }else{
                        naf = true;
                    }
                }
                // Comrpueba si todos los booleanos terminaron en true, si terminaron devuelve un true final, si no, dice que la contraseña no es segura y devuelve false.
                if(my&&mn&&num&&naf){
                    System.out.println(true);
                }else{
                    System.out.println("La contraseña no es segura.");
                    System.out.println(false);
                }
            }else{
                // Si no cumple con los caracteres minimos, no es segura.
                System.out.println("La contraseña no es segura.");
                System.out.println(false);
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }

}
