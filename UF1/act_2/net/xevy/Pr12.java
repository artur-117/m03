import java.util.Scanner;


public class Pr12 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try{
            double preciohora = 30;
            double preciometro = 0.5;
            
            // Obtiene los valores tanto para horas trabajadas como para metros instalados.
            System.out.println("Horas trabajadas:");
            double h = getd();
            System.out.println("Metros instalados:");
            double nm = getd();
    
            // Calcula el total y luego calcula su iva.
            double total = preciohora*h+preciometro*nm;
            double totaliva = total*1.21;
            System.out.println("Has ganado "+total+" € en bruto.");
            System.out.println("Has ganado "+totaliva+" € con iva.");
        }catch(Exception e){
            e.printStackTrace();
        }

        
   }

   public static double getd(){
        // Lee el numero double que se envie por teclado.
        double d = scanner.nextDouble();
        return d;
    }

}
