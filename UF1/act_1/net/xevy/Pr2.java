import java.util.Scanner;

public final class Pr2 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("¿Cual es tu nombre?");
        try{
			//Llama a getl para obtener el nombre, luego registra el producto llamando a la funcion getproducto y lo muestra por pantalla.
            String n = getl();
            System.out.println("¡Hola "+n+"!");
            System.out.println("Dime dos numeros enteros.");
            int producto = getproducto();
            System.out.println(producto);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int getproducto(){
		//Función que calcula el producto de los numeros de la array.
        int producto = 0;
        int[] numeros_int = convertint();
        int l = numeros_int.length;
        for(int i=0;i<l-1;i++){
            if(i==0){
                producto = numeros_int[i]*numeros_int[i+1];
            }else{
                producto *= numeros_int[i+1];
            }
        }
        return producto;
    }

    public static int[] convertint(){
		// Convierte la operacion separada en una array de int.
        String[] numeros = getoperacion();
        int l = numeros.length;
        int[] numeros_int = new int[l];
        for(int i = 0;i<l;i++){
            numeros_int[i] = Integer.parseInt(numeros[i]);
        }
        return numeros_int;

    }

    public static String[] getoperacion(){
		//Obtiene la operacion y la separa en una array.
        String operacion = getl();
        String[] numeros = operacion.split(" ");
        return numeros;
    }

    public static String getl(){
		// Lee lineas.
        String l = scanner.nextLine();
        return l;
    }
}
