package UF1.examen_marzo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import examen_3.Mensajes;

public class Main {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            Boolean menu = true;
            ArrayList<Mensajes> mensajes = new ArrayList<Mensajes>();
            while(menu){
                // Limpia la pantalla y muestra el menu por pantalla..
                System.out.print("\033[H\033[J");
                System.out.println("1. Escribir mensaje.");
                System.out.println("2. Escoger clave.");
                System.out.println("3. Encriptar mensaje.");
                System.out.println("4. Desencriptar mensaje.");
                System.out.println("5. Eliminar registros.");
                System.out.println("6. Salir");
                int opcion = Integer.parseInt(buffer.readLine());
                switch(opcion){
                    // aqui añade un mensaje a la arraylist de mensajes, junto a la clave 0, la clave la debes asignar en la segunda opcion, si no, no encripta.
                    // clave 0 = sin encriptación
                    case 1:
                        System.out.print("\033[H\033[J");
                        Mensajes mensaje = new Mensajes();
                        System.out.print("Escribe tu mensaje:");
                        mensaje.clave=0;
                        mensaje.mensaje_escriptado=buffer.readLine();
                        mensajes.add(mensaje);
                        break;
                    // Selecciona la clave, te sale un menu para hacerlo para todos los registros o para uno solo en concreto.
                    case 2:
                        System.out.print("\033[H\033[J");
                        System.out.println("¿Para que registros quieres escoger la clave nueva?");
                        System.out.println("0. Todos los registros.");
                        System.out.println("1. Seleccionar registro.");
                        
                        switch(Integer.parseInt(buffer.readLine())){
                            case 0:
                                System.out.println("Selecciona la clave nueva: ");
                                int clave = Integer.parseInt(buffer.readLine());
                                // Aplica la clave nueva a todos los registros
                                for(int i = 0; i<mensajes.size(); i++){
                                    mensaje = new Mensajes();
                                    mensaje.clave=clave;
                                    System.out.println("Clave cambiada a "+mensaje.clave);
                                    System.out.println(mensajes.get(i).mensaje_escriptado);
                                    mensaje.mensaje_escriptado = mensajes.get(i).mensaje_escriptado;
                                    mensajes.set(i, mensaje);
                                }
                                break;
                            case 1:
                                for(int i = 0; i<mensajes.size(); i++){
                                    System.out.println(i+"-"+mensajes.get(i).mensaje_escriptado);
                                }
                                System.out.print("Selecciona el registro por id: ");
                                int reg = Integer.parseInt(buffer.readLine());
                                System.out.println("Selecciona la clave nueva: ");
                                mensaje = new Mensajes();
                                mensaje.mensaje_escriptado = mensajes.get(reg).mensaje_escriptado;
                                mensaje.clave = Integer.parseInt(buffer.readLine());
                                // Aplica la clave nueva al registro seleccionado.
                                mensajes.set(reg, mensaje);
                                break;
                        }
                        System.out.println("ENTER");
                        buffer.readLine();
                        break;
                    // Encripta, te sale un menu para hacerlo para todos los registros o para uno solo en concreto.
                    case 3:
                        System.out.print("\033[H\033[J");
                        System.out.println("¿Que registros vamos a encriptar?");
                        System.out.println("0. Todos los registros.");
                        System.out.println("1. Seleccionar registro.");
                        switch(Integer.parseInt(buffer.readLine())){
                            case 0:
                                for(int i = 0; i<mensajes.size(); i++){
                                    mensaje = new Mensajes();
                                    String mensaje_a_encriptar = mensajes.get(i).mensaje_escriptado;
                                    mensaje.mensaje_escriptado = "";
                                    int clave = mensajes.get(i).clave;
                                    // Encripta todo según su clave.
                                    for(int j=0;j<mensaje_a_encriptar.length();j++){
                                        System.out.println(mensaje_a_encriptar.charAt(j));
                                        mensaje.mensaje_escriptado=mensaje.mensaje_escriptado+((char) (mensaje_a_encriptar.charAt(j)+clave));
                                    }
                                    mensaje.clave = clave;
                                    mensajes.set(i, mensaje);
                                }
                                break;
                            case 1:
                                for(int i = 0; i<mensajes.size(); i++){
                                    System.out.println(i+"-"+mensajes.get(i).mensaje_escriptado);
                                }
                                System.out.print("Selecciona el registro por id: ");

                                int reg = Integer.parseInt(buffer.readLine());

                                mensaje = new Mensajes();
                                String mensaje_a_encriptar = mensajes.get(reg).mensaje_escriptado;
                                mensaje.mensaje_escriptado = "";
                                int clave = mensajes.get(reg).clave;
                                for(int j=0;j<mensaje_a_encriptar.length();j++){
                                    mensaje.mensaje_escriptado=mensaje.mensaje_escriptado+((char) (mensaje_a_encriptar.charAt(j)+clave));
                                }
                                mensaje.clave = clave;
                                mensajes.set(reg, mensaje);

                                break;
                        }
                        System.out.println("ENTER");
                        buffer.readLine();
                        break;
                    // Desencripta, te sale un menu para hacerlo para todos los registros o para uno solo en concreto.
                    case 4:
                        System.out.print("\033[H\033[J");
                        System.out.println("¿Que registros vamos a Desencriptar?");
                        System.out.println("0. Todos los registros.");
                        System.out.println("1. Seleccionar registro.");
                        switch(Integer.parseInt(buffer.readLine())){
                            case 0:
                                for(int i = 0; i<mensajes.size(); i++){
                                    System.out.println(mensajes.get(i).mensaje_escriptado+" - "+mensajes.get(i).clave);
                                    String mensaje_a_encriptar = mensajes.get(i).mensaje_escriptado;
                                    String mensaje_desencriptado = "";
                                    int clave = mensajes.get(i).clave;
                                    for(int j=0;j<mensaje_a_encriptar.length();j++){
                                        mensaje_desencriptado=mensaje_desencriptado+((char) (mensaje_a_encriptar.charAt(j)+(-clave)));
                                    }
                                    System.out.println(mensaje_desencriptado);
                                }
                                System.out.println("ENTER");
                                buffer.readLine();
                                break;
                            case 1:
                                for(int i = 0; i<mensajes.size(); i++){
                                    System.out.println(i+"-"+mensajes.get(i).mensaje_escriptado);
                                }
                                System.out.print("Selecciona el registro por id: ");
                                int reg = Integer.parseInt(buffer.readLine());
                                String mensaje_a_encriptar = mensajes.get(reg).mensaje_escriptado;
                                String mensaje_desencriptado = "";
                                int clave = mensajes.get(reg).clave;
                                for(int j=0;j<mensaje_a_encriptar.length();j++){
                                    mensaje_desencriptado=mensaje_desencriptado+((char) (mensaje_a_encriptar.charAt(j)+(-clave)));
                                }
                                System.out.println(mensaje_desencriptado);
                                
                            System.out.println("ENTER");
                            buffer.readLine();
                            break;
                        }
                        break;
                    // Borra, te sale un menu para hacerlo para todos los registros o para uno solo en concreto.
                    case 5:
                        System.out.print("\033[H\033[J");
                        System.out.println("¿Que registros vamos a borrar?");
                        System.out.println("0. Todos los registros.");
                        System.out.println("1. Seleccionar registro.");
                        switch(Integer.parseInt(buffer.readLine())){
                            case 0:
                                // Al entrar aqui y confirmar, borrala todo registro.
                                System.out.println("¡ESTAS A PUNTO DE BORRAR TODO ESCRIBE 'CONFIRMAR' PARA BORRARLO!");
                                if(buffer.readLine().equals("CONFIRMAR")){
                                    for(int i = 0; i<mensajes.size(); i++){
                                        mensajes.remove(i);
                                        System.out.println("Mensaje borrado.");
                                    }
                                }else{
                                    System.out.println("Se ha cancelado el borrado.");
                                }
                                break;
                            case 1:
                                for(int i = 0; i<mensajes.size(); i++){
                                    System.out.println(i+"-"+mensajes.get(i).mensaje_escriptado);
                                }
                                System.out.print("Selecciona el registro por id: ");
                                int reg = Integer.parseInt(buffer.readLine());
                                System.out.println("¡ESTAS A PUNTO DE BORRAR EL MENSAJE"+mensajes.get(reg).mensaje_escriptado+" ESCRIBE 'CONFIRMAR' PARA BORRARLO!");
                                // Se asegura que la confirmacion sea CONFIRMAR.
                                if(buffer.readLine().equals("CONFIRMAR")){
                                    mensajes.remove(reg);
                                    System.out.println("Mensaje borrado.");
                                }else{
                                    System.out.println("Se ha cancelado el borrado.");
                                }
                                break;
                        }


                        System.out.println("ENTER");
                        buffer.readLine();
                        break;
                    // al salir sale que no se guardara, y puedes aceptar la salida escribiendo S.
                    case 6:
                        System.out.print("\033[H\033[J");
                        System.out.println("¡CUIDADO!");
                        System.out.println("Estas a punto de salir del programa, no se va a guardar nada.");
                        System.out.println("¿Estas seguro que deseas salir del programa sin guardar?");
                        System.out.print("S/N: ");
                        // Al escribir S, el menu se vuelve falso, por lo cual no volvera a repetir el bucle del menu y terminara el programa.
                        if(buffer.readLine().equalsIgnoreCase("S")){
                            menu = false;
                        }else{
                            System.out.println("quedaste en el programa.");
                            break;
                        }
                    }
            }     
        }catch(Exception e){
                e.printStackTrace();
        }

    }
}