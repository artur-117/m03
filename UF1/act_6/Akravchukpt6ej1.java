package act_6;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;



public class Akravchukpt6ej1 {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Limpia la pantalla.
            System.out.print("\033[H\033[J");
            // Inicia una variable de resultado en 0, sera el numero con el que empezaremos a sumar..
            int resultado = 0;
            // Muestra el titulo por pantalla y aclara las normas.
            System.out.println("SUMAR Y GANAR");
            System.out.println("Vaya sumando todos los números que le iré diciendo.");
            System.out.println("Empezamos por "+resultado);
            // Inicia una variable aleatoria, la usaremos para generar las sumas.
            Random aleatorio = new Random();
            // Inicia el boolean sumando en true, significa que se esta sumando, si falla dejara de sumar.
            Boolean sumando = true;
            // Inicia una variable int que ira almacenando el numero aleatorio generado.
            int numero = 0;
            // Inicia una variable int que servira para la escritura del resultado.
            int numerores = 0;
            // Inicia un bucle, el bucle funcionara mientras estes sumando correctamente.
            while(sumando){
                // Genera el numero aleatorio.
                numero = aleatorio.nextInt(51);
                // Muestra la pregunta por pantalla.
                System.out.print("Más "+numero+": ");
                // Te permite responder, si respondes bien, sigues, si no, pierdes y termina el juego.
                numerores = Integer.parseInt(buffer.readLine());
                if(resultado+numero==numerores){
                    resultado = resultado+numero;
                }else{
                    sumando = false;
                }
            }
            // Si el resultado fue de 0, te devuelve un texto, si no, te devuelve otro.
            if(resultado==0){
                System.out.println("¿Ha entendido el juego?");
            }else{
                System.out.println("¡Repase las sumas antes de jugar!");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
