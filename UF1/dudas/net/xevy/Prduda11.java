import java.util.Scanner;

public class Prduda11 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // Sale por pantalla las opciones.
        System.out.println("¿Por quien votas?");
        System.out.println("1 - Partido Azul");
        System.out.println("2 - Partido Rojo");
        System.out.println("3 - Partido Verde");
        System.out.println("Seleccion un numero.");
            // Comprueba si el numero entero obtenido coincide con una opcion y muestra a quien ha votado.
            switch(scanner.nextInt()){
                case 1:
                    System.out.println("¡Has votado por los azules!");
                    break;
                case 2:
                    System.out.println("¡Has votado por los rojos!");
                    break;
                case 3:
                    System.out.println("¡Has votado por los verdes!");
                    break;
                default:
                    System.out.println("¡No has votado nada!");
                    break;
            }
    }
}
