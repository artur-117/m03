import java.util.Scanner;

public final class Pr1 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Introduce un numero entero.");
        try{
			//Llama a la función getc para obtener el cuadrado.
            int c = getc();
            System.out.println(c);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int getc(){
		// Esta funcion llama a getn para obtener el numero entero y luego calcula su cuadrado.
        int n = getn();
        int c = n*n;
        return c;
    }

    public static int getn(){
        int n = scanner.nextInt();
        return n;
    }
}
