package act_repaso;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukptrepasoej2 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            Random random = new Random();
            int numero_multis = Integer.parseInt(buffer.readLine());
            if(0<numero_multis){
                for(int i = 0; i<numero_multis; i++){
                    int a = random.nextInt(9)+2;
                    int b = random.nextInt(9)+2;
                    System.out.println("¿Cuanto es "+a+" x "+b+"?");
                    System.out.println((Integer.parseInt(buffer.readLine())==a*b)?"¡RESPUESTA CORRECTA!":"¡RESPUESTA INCORRECTA!");
                }
            }else{
                System.out.println("¡El numero de preguntas debe ser al menos 1!");
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
