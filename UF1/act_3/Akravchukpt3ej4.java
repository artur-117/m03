public class Akravchukpt3ej4 {
    public static void main(String[] args) {
        // Asigna una variable int con un 0 inicial, usaremos esta variable para almacenar el numero de multiplos que tiene el 7.
        int esm = 0;
        // Bucle que detecta todos los multiplos de 7 entre los primeros mil numeros.
        for (int i = 7; i <= 1000; esm += ++i % 7 == 0 ? 1 : 0);
        // Mostramos multiples
        System.out.println(String.format("Hay %d multiplos.", esm));
    }
}
