package act_refuerzo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class NumerosReales {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            ArrayList<Integer> numeroreal = new ArrayList<Integer>();
            boolean menu = true;
            while(menu){
                System.out.print("\033[H\033[J");
                System.out.println("1 - Agregar número real.");
                System.out.println("2 - Buscar número real.");
                System.out.println("3 - Modificar número real.");
                System.out.println("4 - Eliminar número real.");
                System.out.println("5 - Insertar número real en posicion.");
                System.out.println("6 - Salir.");
                switch(Integer.parseInt(buffer.readLine())){
                    case 1:
                        System.out.println("Agrega un numero: ");
                        numeroreal.add(Integer.parseInt(buffer.readLine()));
                        break;
                    case 2:
                        System.out.print("\033[H\033[J");
                        System.out.println("1 - Buscar todos.");
                        System.out.println("2 - Buscar por posicion.");
                        System.out.println("3 - Buscar por número real.");
                        switch(Integer.parseInt(buffer.readLine())){
                            case 1:
                                for(int i = 0; i<numeroreal.size();i++){
                                    System.out.println(i+" - "+numeroreal.get(i));
                                }
                                break;
                            case 2:
                                int posbusqueda = Integer.parseInt(buffer.readLine());
                                if(numeroreal.size()>posbusqueda){
                                    System.out.println(numeroreal.get(posbusqueda));
                                }else{
                                    System.out.println("Te has pasado del limite...");
                                }
                                break;
                            case 3:
                                int numrealbusqueda = Integer.parseInt(buffer.readLine());
                                boolean numencontrado = false;
                                for(int i = 0; i<numeroreal.size(); i++){
                                    if(numeroreal.get(i).equals(numrealbusqueda)){
                                        System.out.println("¡Número real buscado "+numrealbusqueda+"encontrado en la posición "+i+"!");
                                        numencontrado=true;
                                        break;
                                    }
                                }
                                if(!numencontrado){
                                    System.out.println("¡El número real solicitado no fue encontrado!");
                                    break;
                                }
                        }
                        buffer.readLine();
                        break;
                    case 3:
                        System.out.print("\033[H\033[J");
                        System.out.println("1 - Modificar por posicion.");
                        System.out.println("2 - Modificar por número real.");
                        switch(Integer.parseInt(buffer.readLine())){
                            
                            case 1:
                                int posbusqueda = Integer.parseInt(buffer.readLine());
                                if(numeroreal.size()>posbusqueda){
                                    System.out.println(numeroreal.get(posbusqueda));
                                    System.out.println("Escribe el nuevo número real con el que quiere modificar esta posición.");
                                    numeroreal.set(posbusqueda, Integer.parseInt(buffer.readLine()));
                                }else{
                                    System.out.println("Te has pasado del limite...");
                                    System.out.println("Pulsa cualquier tecla para seguir.");
                                    buffer.readLine();
                                }
                                break;
                            case 2:
                                int numrealbusqueda = Integer.parseInt(buffer.readLine());
                                boolean numencontrado = false;
                                for(int i = 0; i<numeroreal.size(); i++){
                                    if(numeroreal.get(i).equals(numrealbusqueda)){
                                        System.out.println("¡Número real buscado "+numrealbusqueda+" encontrado en la posición "+i+"!");
                                        numencontrado=true;
                                        System.out.println("Escribe el nuevo número real con el que quiere modificar esta posición.");
                                        numeroreal.set(i, Integer.parseInt(buffer.readLine()));
                                        break;
                                    }
                                }
                                if(!numencontrado){
                                    System.out.println("¡El número real solicitado no fue encontrado! Pulsa cualquier tecla para seguir.");
                                    buffer.readLine();
                                }
                                break;
                        }
                        break;
                    case 4:
                        System.out.print("\033[H\033[J");
                        System.out.println("1 - Eliminar por posicion.");
                        System.out.println("2 - Eliminar por número real.");
                        switch(Integer.parseInt(buffer.readLine())){
                            case 1:
                                int posbusqueda = Integer.parseInt(buffer.readLine());
                                if(numeroreal.size()>posbusqueda){
                                    System.out.println(numeroreal.get(posbusqueda));
                                    System.out.println("Escribe CONFIRMAR para borrar el numero encontrado de la posición "+posbusqueda+".");
                                    if(buffer.readLine().equals("CONFIRMAR")){
                                        numeroreal.remove(posbusqueda);
                                    }else{
                                        System.out.println("No has confirmado correctamente, pulsa cualquier tecla para seguir.");
                                        buffer.readLine();
                                    }
                                }else{
                                    System.out.println("Te has pasado del limite...");
                                }
                                break;
                            case 2:
                                int numrealbusqueda = Integer.parseInt(buffer.readLine());
                                boolean numencontrado = false;
                                for(int i = 0; i<numeroreal.size(); i++){
                                    if(numeroreal.get(i).equals(numrealbusqueda)){
                                        System.out.println("¡Número real buscado "+numrealbusqueda+" encontrado en la posición "+i+"!");
                                        numencontrado=true;
                                        System.out.println("Escribe CONFIRMAR para borrar el numero encontrado de la posición "+i+".");
                                        if(buffer.readLine().equals("CONFIRMAR")){
                                            numeroreal.remove(i);
                                        }else{
                                            System.out.println("No has confirmado correctamente, pulsa cualquier tecla para seguir.");
                                            buffer.readLine();
                                        }
                                        break;
                                    }
                                }
                                if(!numencontrado){
                                    System.out.println("¡El número real solicitado no fue encontrado!");
                                }
                                break;
                        }
                        break;
                    case 5:
                        System.out.print("\033[H\033[J");
                        System.out.println("Escribe una posición: ");
                        int pos = Integer.parseInt(buffer.readLine());
                        System.out.println("Agrega un numero: ");
                        numeroreal.add(pos,Integer.parseInt(buffer.readLine()));
                        break;
                    case 6:
                        menu = false;
                        System.out.println("Saliste.");
                        break;
                }
            }
        }catch(Exception e){
            // si llega aqui hubo fallo.
            e.printStackTrace();

        }
    }
}