import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt3ej3 {
    public static void main(String[] args) {
        // Inicia un try junto al bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Inicia todas las variables necesarias y lee el tamaño inicial de la bacteria.
            System.out.print("Tamaño inicial de la bacteria: ");
            double bacteria = Double.parseDouble(buffer.readLine());
            double intentos = 0.0;
            double hora = 0.0;

            // Mientras la bacteria sea menor a 10000000, sigue aumentando hasta llegar a más de 10000000, ahí muere.
            for(int i = 1; bacteria<10000000.0; i++){
                bacteria = bacteria * 2.0;
                intentos=i;
                hora = (intentos*3.0);
                System.out.println("Tu bacteria crecio a "+bacteria+" en "+hora+" minutos.");
            }
            // Convierte a horas y aclara la hora de su muerte.
            hora = (intentos*3.0)/60;
            System.out.println("Tu bacteria murio en "+hora+" horas.");
        }catch(Exception e) {
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }                
    }
}
