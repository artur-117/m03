import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;


public class Akravchukpt3ej1 {
    public static void main(String[] args) {
        // Inicia un try junto al BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Establece la variable desactivador a 0, esta variable se usara más adelante para crear un bucle. También iniciamos n1 y n2 para hacer calculos.
            int desactivador = 0;
            double n1=0;
            double n2=0;
            // Establecemos el formato en 0.#, lo que significa que si el resultado es 5.0 se mostrara como 5 sin el .0, si es 5.5, se muestra normal como 5.5.
            DecimalFormat format = new DecimalFormat("0.#");
            // Iniciamos el bucle, mientras el desactivador sea 0, sera solo 1 si activamos la opcion 0.
            while(desactivador==0){
                // Limpia la pantalla.
                System.out.print("\033[H\033[J");
                
                // Muestra todo el menu por pantalla.
                System.out.println("Menu Principal");
                System.out.println();
                System.out.println("1 - Sumar");
                System.out.println("2 - Restar");
                System.out.println("3 - Multiplicar");
                System.out.println("4 - Dividir");
                System.out.println("0 - Salir");
                System.out.println();   

                // Te permite seleccionar una opción.
                System.out.print("Opción: ");
                int opcion = Integer.parseInt(buffer.readLine());

                // Si la opcion es 1-4, te pide dos numeros para calcularlos.
                if(0<opcion&&opcion<5){
                    System.out.print("Primer numero:");
                    n1 = Double.parseDouble(buffer.readLine());
                    System.out.print("Segundo numero:");
                    n2 = Double.parseDouble(buffer.readLine());
                } 

                // Switch con los casos para distintas opciones.
                switch(opcion){
                    // Aqui se sumara.
                    case 1:
                        System.out.println(format.format(n1+n2));
                        buffer.readLine();
                        break;
                    // Aqui se restara.
                    case 2:
                        System.out.println(format.format(n1-n2));
                        buffer.readLine();
                        break;
                    // Aqui se multiplicara.
                    case 3:
                        System.out.println(format.format(n1*n2));
                        System.out.println();
                        buffer.readLine();
                        break;
                    // Aqui se dividira.
                    case 4:
                        System.out.println(format.format(n1/n2));
                        buffer.readLine();
                        break;
                    // Aqui se desactiva el bucle.
                    case 0:
                        desactivador=1;
                        break;
                    // En caso de que la opción seleccionada no coincida con la lista de casos, da fallo y te permite volver a intentarlo. 
                    default:
                        System.out.println("Hubo un fallo de opción... ¡Vuelve a intentarlo!");
                        break;
                }
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}