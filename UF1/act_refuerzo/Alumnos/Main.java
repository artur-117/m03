package act_refuerzo.Alumnos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));


    public static Comparator<Alumnos> CompNotaAlumnos = new Comparator<Alumnos>() {
        public int compare(Alumnos s1, Alumnos s2){
            int nota1 = s1.nota;
            int nota2 = s2.nota;
            return nota2-nota1;
        }
    };

    public static void main(String[] args) throws NumberFormatException, IOException {
        // Inicia un try junto a un bufferedreader.
        try
        {
            ArrayList<Alumnos> alumnos = new ArrayList<Alumnos>();
            boolean menu = true;
            while(menu){ 
                System.out.println("\033c");
                System.out.println("1 - Agregar Alumnos.");
                System.out.println("2 - Evaluar Alumnos.");
                System.out.println("3 - Mostrar Alumnos.");
                System.out.println("4 - Salir.");
                switch(Integer.parseInt(buffer.readLine())){
                    case 1:
                        System.out.println("Escribe el nombre del alumno. Cuando quieras detenerte escribe un 0.");
                        System.out.print("Nombre:");
                        String nombrealumno = buffer.readLine();
                        while(!nombrealumno.equals("0")){
                            if(validacion_nombre(nombrealumno) && noduplicado(alumnos, nombrealumno)){
                                Alumnos alumno = new Alumnos();
                                alumno.Nombre = nombrealumno;
                                alumno.nota = 0;
                                alumnos.add(alumno);
                            }
                            System.out.print("Nombre:");
                            nombrealumno = buffer.readLine();
                        }
                        break;
                    case 2:
                        for(int i = 0; i<alumnos.size();i++){
                            System.out.print("Asigna una nota para "+alumnos.get(i).Nombre+": ");
                            Alumnos alumno = new Alumnos();
                            alumno.Nombre = alumnos.get(i).Nombre;
                            alumno.nota = Integer.parseInt(buffer.readLine());
                            alumnos.set(i, alumno);
                        }
                        
                        
                    
                    case 3:
                        for(int i = 0; i<alumnos.size();i++){
                            Collections.sort(alumnos, Main.CompNotaAlumnos);
                            System.out.println(alumnos.get(i).Nombre+" "+alumnos.get(i).nota);
                        }
                        buffer.readLine(); 
                        break;
                    case 4:
                        menu = false;
                        break;
                }
            }
        }catch(Exception e){
            e.printStackTrace(); 
        }
    }

    public static boolean noduplicado(ArrayList<Alumnos> alumnos, String nombre) throws IOException{
        for(int i = 0; i<alumnos.size();i++){
                if(alumnos.get(i).Nombre.equalsIgnoreCase(nombre)){
                    System.out.println(alumnos.get(i).Nombre+" es igual a "+nombre);
                    System.out.println("¿Seguro quieres añadir el nombre de igual manera? Escribe 'CONFIRMAR' para confirmarlo o cualquier otra cosa para rechazarlo.");
                    if(buffer.readLine().equals("CONFIRMAR")){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return true;
                }
        }
        return true;
    }

    public static boolean validacion_nombre(String nombre) throws IOException {
        Boolean nc = true;
        int mincar = 3;
        int maxcar = 22;
        if(mincar<=nombre.length()&&nombre.length()<=maxcar){
            for(int i = 0; i<nombre.length();i++){
                if(nombre.charAt(i)>='a' && nombre.charAt(i)<='z'||nombre.charAt(i)>='A' && nombre.charAt(i)<='Z'){
                }else{
                    nc = false;
                    System.out.println("El nombre debe contener solo letras.");
                    break;
                }
            }
            if(nc){
                return true;
            }else{
                return false;
              }
        }else{
            if(nombre.length()<mincar){
                System.out.println("El nombre debe contener almenos "+mincar+" caracteres.");
            }else{
                System.out.println("El nombre debe contener como maximo "+maxcar+" caracteres.");
            }
            return false;
        }
    }
}