import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arturkravchukex1 {
    public static void main(String[] args) {
        System.out.print("Escribe una cantidad de días:");
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Guarda un valor int en d escrito por teclado, se usara para los dias.
            int d = Integer.parseInt(buffer.readLine());

            // Comprueba si es positivo.
            if(0<d){
                // Inicia las variables de dias mayos [Kin, Uinal, Tun, Katún, Baktún y dtotal para mostrar los dias totales].
                int kin = 0;
                int uinal = 0;
                int tun = 0;
                int katún = 0;
                int baktún = 0;
                int dtotal = d;

                // Comprueba si dias es mayor a 144000 (1 baktún).
                if(144000<=d){
                    // guarda los baktúnes dividiendo dias por la cantidad de dias que tiene 1 baktún.
                    baktún = d/144000;
                    // resta a dias los baktúnes.
                    d=d-(baktún*144000);
                }
                // Comprueba si dias es mayor a 7200 (1 katún.).
                if(7200<=d){
                    // guarda los katúnes dividiendo dias por la cantidad de dias que tiene 1 katún.
                    katún = d/7200;
                    // resta a dias los katúnes.
                    d=d-(katún*7200);
                }
                // Comprueba si dias es mayor a 360 (1 tun.).
                if(360<=d){
                    // guarda los tunes dividiendo dias por la cantidad de dias que tiene 1 tun.
                    tun = d/360;
                    // resta a dias los tunes.
                    d=d-(tun*360);
                }
                // Comprueba si dias es mayor a 20 (1 uinal.).
                if(20<=d){
                    // guarda los uinales diviendo dias por la cantidad de dias que tiene 1 uinal.
                    uinal = d/20;
                    // resta a dias los uinales.
                    d=d-(uinal*20);
                }

                // Comprueba si dias es mayor a 1 (1 kin.).
                if(1<=d){
                    // guarda los kines diviendo dias por la cantidad de dias que tiene 1 kin.
                    kin=d/1;
                    // resta a dias los kines.
                    d=d-(kin*1);
                }
                // Muestra el resultado final.
                System.out.println(dtotal+" dias son "+baktún+" baktún,"+katún+" katún,"+tun+" tun,"+uinal+" uinal,"+kin+" kin.");
            }else{
                System.out.println("Por favor, no escriba números negativos.");
            }
        } catch (Exception e) {
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
