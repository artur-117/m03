import java.util.Scanner;

public final class Pr3 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
		// Muestra un menu por pantalla.
        System.out.println("__Menu__");
        System.out.println("1 - Perimetro y Area de un Rectangulo.");
        System.out.println("2 - Perimetro y Area de un Circulo");
        System.out.println("3 - Volumen de una Esfera");
        try{
			//Selecciona una opción del menu y dependiendo de la opción ejecuta una función para guardar sus resultados y mostrarlos por pantalla.

            int opcion = geti();
            if(opcion==1){
                int[] resultado = getperimetro_area_rectangulo();
                System.out.println("El perimetro es "+resultado[0]+" y la area es "+resultado[1]);    
            }else if(opcion==2){
                double[] resultado = getperimetro_area_circulo();
                System.out.println("El perimetro es "+resultado[0]+" y la area es "+resultado[1]);
            }else if(opcion==3){
                double resultado = getvolumen_esfera();
                System.out.println("El volumen es "+resultado); 
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int[] getperimetro_area_rectangulo(){
		// Obtiene el perimetro y area de un rectangulo.
        int[] resultado = new int[2];
        System.out.println("Base");
        int b = geti();
        System.out.println("Altura");
        int h = geti();
        int l = 2*h+2*b;
        int a = b*h;
        resultado[0] = l;
        resultado[1] = a;
        return resultado;
    }

    public static double[] getperimetro_area_circulo(){
		// Obtiene el perimetro y area de un circulo.
        double[] resultado = new double[2];
        System.out.println("Radio");
        double r = geti();
        resultado[0] = Math.PI*r*r;
        resultado[1] = 2*Math.PI*r;
        return resultado;
    }

    public static double getvolumen_esfera(){
		// Obtiene el volumen de una esfera.
        System.out.println("Radio");
        int r = geti();
        double resultado = 4/3*Math.PI*r*r*r;
        return resultado;
    }

    public static int geti(){
		// Obtiene un numero entero.
        int i = scanner.nextInt();
        return i;
    }
}
