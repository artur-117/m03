// Haz un juego de guerra, en el que tomas control de un pais y puedes ir conquistando otros paises, tendras que evitar
// que conquisten tus provincias o que te tiren una bomba nuclear.
// Debes de hacer una lista con todas las provincias y del pais propietario de la provincia, los paises van ordenados por id.
// Cada país debe tener una provincia capital.
// También debes de crear los lideres de los paises, su ideologia, los paises que tienen armamento nuclear, su economia, etc.
// Ganas una vez que hayas conquistado todos los paises.
// Pierdes al perder tu capital.   
package UF2.act_3; 

import java.util.ArrayList;
import java.util.Random;


public class Akravchukpt3ej1 {
    public static void main(String[] args) {

        menuINI();
        if(Integer.parseInt(System.console().readLine())==1){


            Random random = new Random();

            ArrayList<Paises> paises = new ArrayList<Paises>();
            String[] array_paises = {"Afganistán","Albania","Alemania","Andorra","Angola","Antigua y Barbuda","Arabia Saudita","Argelia","Argentina","Armenia","Australia","Austria","Azerbaiyán","Bahamas","Bangladés","Barbados","Baréin","Bélgica","Belice","Benín","Bielorrusia","Birmania","Bolivia","Bosnia y Herzegovina","Botsuana","Brasil","Brunéi","Bulgaria","Burkina Faso","Burundi","Bután","Cabo Verde","Camboya","Camerún","Canadá","Catar","Chad","Chile","China","Chipre","Ciudad del Vaticano","Colombia","Comoras","Corea del Norte","Corea del Sur","Costa de Marfil","Costa Rica","Croacia","Cuba","Dinamarca","Dominica","Ecuador","Egipto","El Salvador","Emiratos Árabes Unidos","Eritrea","Eslovaquia","Eslovenia","España","Estados Unidos","Estonia","Etiopía","Filipinas","Finlandia","Fiyi","Francia","Gabón","Gambia","Georgia","Ghana","Granada","Grecia","Guatemala","Guyana","Guinea","Guinea ecuatorial","Guinea-Bisáu","Haití","Honduras","Hungría","India","Indonesia","Irak","Irán","Irlanda","Islandia","Islas Marshall","Islas Salomón","Israel","Italia","Jamaica","Japón","Jordania","Kazajistán","Kenia","Kirguistán","Kiribati","Kuwait","Laos","Lesoto","Letonia","Líbano","Liberia","Libia","Liechtenstein","Lituania","Luxemburgo","Madagascar","Malasia","Malaui","Maldivas","Malí","Malta","Marruecos","Mauricio","Mauritania","México","Micronesia","Moldavia","Mónaco","Mongolia","Montenegro","Mozambique","Namibia","Nauru","Nepal","Nicaragua","Níger","Nigeria","Noruega","Nueva Zelanda","Omán","Países Bajos","Pakistán","Palaos","Palestina","Panamá","Papúa Nueva Guinea","Paraguay","Perú","Polonia","Portugal","Reino Unido","República Centroafricana","República Checa","República de Macedonia","República del Congo","República Democrática del Congo","República Dominicana","República Sudafricana","Ruanda","Rumanía","Rusia","Samoa","San Cristóbal y Nieves","San Marino","San Vicente y las Granadinas","Santa Lucía","Santo Tomé y Príncipe","Senegal","Serbia","Seychelles","Sierra Leona","Singapur","Siria","Somalia","Sri Lanka","Suazilandia","Sudán","Sudán del Sur","Suecia","Suiza","Surinam","Tailandia","Tanzania","Tayikistán","Timor Oriental","Togo","Tonga","Trinidad y Tobago","Túnez","Turkmenistán","Turquía","Tuvalu","Ucrania","Uganda","Uruguay","Uzbekistán","Vanuatu","Venezuela","Vietnam","Yemen","Yibuti","Zambia","Zimbabue"};
            for(int i = 0; i<array_paises.length;i++){
                Paises pais = new Paises();
                pais.id = i;
                pais.pais = array_paises[i];
                pais.num_provincias = random.nextInt(40)+5;
                pais.poblacion = pais.num_provincias*(random.nextInt(10000)+2500);
                paises.add(pais);
            }
    
            System.out.println("id  Pais");
            System.out.println("========================");
            for (Paises paises2 : paises) {
                System.out.println(paises2.id+"\t"+paises2.pais+"\nProvincias: "+paises2.num_provincias+" Poblacion:"+paises2.poblacion);
                System.out.println("========================");
            }
    
            System.out.println("");


            menuDOS();

            
        }else{
            System.out.println("FIN");
        }





    }

    public static void menuINI(){
        System.out.println("1 - Empezar a Jugar.");
        System.out.println("2 - Salir");
    }

    public static void menuDOS(){
        System.out.println("1 - Seleccionar país");
        System.out.println("2 - País aleatorio");
        System.out.println("3 - Ajustes de año");
    }

}