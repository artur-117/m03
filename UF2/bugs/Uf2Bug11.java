import java.util.ArrayList;
import java.util.Scanner;
public class Uf2Bug11 {
    /*Este programa es un Menu para añadir coches en un parking, donde tendremos opciones diferentes del 1 al 6 para casos distintos. En el
     podremos hacer una inserción de datos de un coche, consultar por nombre, consultar por tipo de vehiculo, consultar todos los vehiculos y dar de baja al vehiculo.
     */
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        int trobat = 0;
        String opcion ,resposta = "";
        Parking parking = new Parking();
        //Creamos un menu con un bucle do-while
        do{
            System.out.println("1.Introduir vehicle");
            System.out.println("2.Consultar dades d'un vehicle per tipus"); 
            System.out.println("3. Consultar dades d’un vehicle per nom del propietari del vehicle.");                
            System.out.println("4. Llistar tots els vehicles.");  
            System.out.println("5. Donar de baixa un vehicle."); 
            System.out.println("6. Sortir ");
            System.out.println("");  
            System.out.println("¿Dime la opción que deseas?");
            opcion = x.next();
        //Para insertar un vehiculo
        if(opcion.equalsIgnoreCase("1")){
            if(parking.vehicles.size()<20){
            Vehicle vehicle = new Vehicle();
            System.out.println("Quin tipus de vehicle és?");
            // añadi los tipos posibles, no se veia nada...
            System.out.println("turismes\nmotos\nautocaravanes\nfurgonetes\ncamions");
            vehicle.tipus = x.next();
            if(vehicle.tipus.equals("turismes")|| vehicle.tipus.equalsIgnoreCase("motos") || vehicle.tipus.equalsIgnoreCase("autocaravanes")||vehicle.tipus.equalsIgnoreCase("furgonetes")||vehicle.tipus.equalsIgnoreCase("camions")){
                System.out.println("Quin és el nom del propietari?");
                vehicle.nom = x.next();
                System.out.println("Quina és la matricula?");
                vehicle.matricula = x.next();
                parking.vehicles.add(vehicle);
            }
            else{
                System.out.println("No s'ha pogut introduir el vehicle.");
            }
            }  
        }
        //Para consultar por tipo de vehiculo
        else if(opcion.equalsIgnoreCase("2")){
            trobat = 0;
            System.out.println("Quin és el tipus del vehicle?");
            // añadi los tipos posibles, no se veia nada...
            System.out.println("turismes\nmotos\nautocaravanes\nfurgonetes\ncamions");
            resposta = x.next();
            for(Vehicle vehicle : parking.vehicles){
                if(resposta.equals(vehicle.tipus)){
                    trobat = 1;
                    System.out.println("El nom del propietari és: "+vehicle.nom+". El tipus del vehicle és "+vehicle.tipus+". La matrícula del vehicle és: "+vehicle.matricula+".");
                }
            }
            if(trobat!=1){
                System.out.println("No hi ha cap classe de vehicle ara mateix amb cita a les nostres instal·lacions");
            }
        }
        //Para consultar por nombre de propietario del vehiculo
        else if(opcion.equalsIgnoreCase("3")){
            System.out.println("Quin és el nom del propietari del vehicle?");
            resposta = x.next();
                for(Vehicle vehicle : parking.vehicles){
                    if(resposta.equals(vehicle.nom)){
                        trobat = 1;
                        System.out.println("El nom del propietari és: "+vehicle.nom+". El tipus del vehicle és "+vehicle.tipus+". La matrícula del vehicle és: "+vehicle.matricula+".");
                    }
                }
                if(trobat!=1){
                    System.out.println("No hi ha cap vehicle amb aquest nom de propietari.");
                }
        }
        //Para consultar todos los datos de la base de datos del parking
        else if(opcion.equalsIgnoreCase("4")){
            for(Vehicle vehicle : parking.vehicles){
                System.out.println("------------------");
                System.out.println("---El nom del propietari és: "+vehicle.nom+".---");
                System.out.println("---El tipus del vehicle és "+vehicle.tipus+".---");
                System.out.println("---La matrícula del vehicle és: "+vehicle.matricula+".---");
                System.out.println("------------------");
            }
        }
        //Para borrar un vehiculo y sus datos de la base de datos.
        else if(opcion.equalsIgnoreCase("5")){
            System.out.println("Quin vehicle vol esborrar? Introdueixi la matrícula");
            String matricula = x.next();
            System.out.println("Estàs segur de fer-ho? No hi haurà marxa enrere.(Posa 'si' si estás segur)");
            resposta = x.next();
            if(resposta.equalsIgnoreCase("si")){
            	for(Vehicle vehicle : parking.vehicles){
                    if(matricula.equals(vehicle.matricula)){
                        parking.vehicles.remove(vehicle);
                        System.out.println("El vehicle amb matrícula: "+vehicle.matricula+" ha estat esborrat de la base de dades.");
                        // faltaba el break;
                        break;
                    }
            	}
            }
            else{
                System.out.println("El vehicle no s'ha esborrat.");
            }
        }
        else{
            // Cambiado de pon una opcion del 1 al 6, a programa finalizado.
            //System.out.println("Posa una opció del 1 al 6.");
            System.out.println("Programa finalizado.");
        }
        }
        while(!opcion.equalsIgnoreCase("6"));
    }
}
class Parking{
    ArrayList<Vehicle> vehicles = new ArrayList<>();
}
class Vehicle{
    String nom;
    String tipus;
    String matricula;
}
