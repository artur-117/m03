import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt3ej9 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Lee la cantidad de numeros que se van a introducir en una variable.
            System.out.print("¿Cuantos numeros introduciras? ");
            int x = Integer.parseInt(buffer.readLine());
            // Inicia la variable negativa.
            int negativo = 0;
            // Un bucle que te pide un numero y va detectando si el numero leido es negativo o no, si es negativo, se suma 1 a la variable negativo.
            for(int i = 0; i<x; i++){
                System.out.print("Dame el numero "+(i+1)+":");
                if(Integer.parseInt(buffer.readLine())<0){
                    negativo++;
                }
            }
            // Muestra el total de negativos.
            System.out.println("Has escrito "+negativo+" numeros negativos.");
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}