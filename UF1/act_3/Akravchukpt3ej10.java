public class Akravchukpt3ej10 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try{
            // Bucles que siguen una serie de numeros.
            // Muestra los numeros del 1 al 10 sumando de 1 en 1.
            for(int i = 0; i < 10; System.out.print((i++<10)?(i)+"  ":(i)+" "));
            System.out.println();
            // Muestra los numeros del 2 al 20 sumando de 2 en 2.
            for(int j = 0; j < 20; System.out.print(((j=j+2)<10)?(j)+"  ":(j)+" "));
            System.out.println();
            // Muestra los numeros del 20 al 38 sumando de 2 en 2.
            for(int x = 18; x < 38; System.out.print(((x=x+2)<10)?(x)+"  ":(x)+" "));
            System.out.println();
            // Muestra los numeros del 10 al 30 sumando de 4 en 4.
            for(int c = 6; c < 30; System.out.print(((c=c+4)<10)?(c)+"  ":(c)+" "));
            System.out.println();
            // Muestra los numeros del 40 al 0 restando de 5 en 5.
            for(int m = 45; m > 0; System.out.print(((m=m-5)<10)?(m)+"  ":(m)+" "));
            System.out.println();
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}