package act_6;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukpt6ej3 {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Limpia la pantalla.
            System.out.print("\033[H\033[J");
            System.out.println("JUEGO DE DADOS");
            System.out.print("¿Cuántos turnos va a tener la partida? ");
            // Inicia la variable de los turnos permitiendote elegir cuantos turnos tendra.
            int turnos = Integer.parseInt(buffer.readLine());
            if(0<turnos){
                // Inicia una variable aleatoria.
                Random aleatorio = new Random();
                // Inicia los dados, y sus victorias.
                int vc = 0;
                int[] dcubitus = new int[4]; 
                int vh = 0;
                int[] dhumerus = new int[3]; 
                // Inicia el bucle con los turnos.
                for(int i = 0; i<turnos;i++){
                    //Inicia a 0 la puntuacion de los  jugadores.
                    int Cubitus = 0;
                    int Humerus = 0;
                    // Muestra el turno.
                    System.out.println("Turno "+i);
                    // Tira los dados de cubitus.
                    for(int c = 0; c<dcubitus.length;c++){
                        dcubitus[c]=aleatorio.nextInt(7);
                        if(dcubitus[c]==0){
                            dcubitus[c]=dcubitus[c]+1;
                        }
                    }
                    // Tira los dados de humerus.
                    for(int c = 0; c<dhumerus.length;c++){
                        dhumerus[c]=aleatorio.nextInt(7);
                        if(dhumerus[c]==0){
                            dhumerus[c]=dhumerus[c]+1;
                        }
                    }
                    // Muestra por pantalla los dados tirados y su puntuación, la puntuación sale de los dados impares.
                    System.out.print("Turno de Cubitus: ");
                    for(int c = 0; c<dcubitus.length;c++){
                        if(dcubitus[c]%2==0){
                        }else{
                            Cubitus=Cubitus+dcubitus[c];
                        }
                        System.out.print(dcubitus[c]+" ");
                    }
                    System.out.print("=> "+Cubitus+" puntos");
                    System.out.println();

                    // Muestra por pantalla los dados tirados y su puntuación, la puntuación sale de los dados pares.
                    System.out.print("Turno de Humerus: ");
                    for(int c = 0; c<dhumerus.length;c++){
                        if(dhumerus[c]%2==0){
                            Humerus=Humerus+dhumerus[c];
                        }
                        System.out.print(dhumerus[c]+" ");
                    }
                    System.out.print("=> "+Humerus+" puntos");
                    
                    // Detecta quien gano este turno y le da puntos para la puntuación final que decidira la victoria.
                    if(Cubitus<Humerus){
                        vh++;
                    }else{
                        vc++;
                    }

                    System.out.println();
                }

                // Devuelve los resultados finales.
                if(vc==vh){
                    System.out.println("Han empatado (a "+vc+")");
                }else if(vc<vh){
                    System.out.println("Ha ganado Humerus ("+vh+" a "+vc+")");
                }else{
                    System.out.println("Ha ganado Cubitus ("+vc+" a "+vh+")");
                }

            }else{
                // Si los turnos están en negativo, devuelve fallo.
                System.out.println("¡La partida debe tener al menos un turno!");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
