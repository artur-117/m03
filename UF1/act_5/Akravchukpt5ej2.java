package act_5;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukpt5ej2 {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Inicia un booleano de bucle con un true, lo usaremos para repetir el programa en bucle hasta que el usuario decida detenerse.
            boolean bucle = true;
            // Inicia una variable aleatoria.
            Random aleatorio = new Random();
            // Empieza un bucle con la condicion booleana del bucle, se detendra cuando el bucle cambie a falso.
            while(bucle){
                // Limpia la pantalla y muestra NUEVO por pantalla.
                System.out.print("\033[H\033[J");
                System.out.println("NUEVO");
                // Inicia el dado y la puntuacion de los jugadores.
                int dado = 0;
                int Anna = 9;
                int Bernat = 9;
                // Bucle que no se detendra hasta que una puntuacion baje a menos de 0.
                while(0<Anna&&0<Bernat){
                    // Muestra el dado que ha caido y comprueba con condiciones a quien retirarle los puntos y a quien sumarle.
                    System.out.println("El dado caido es: "+(((dado=aleatorio.nextInt(7))==0)?1:dado));
                    if(dado%2==0){
                        Anna=Anna-dado;
                        Bernat=Bernat+dado;
                    }else{
                        Bernat=Bernat-dado;
                        Anna=Anna+dado;
                    }
                    // Muestra los resultados de esta ronda, si nadie esta por debajo de 0, el juego sigue.
                    System.out.println("Anna:"+Anna);  
                    System.out.println("Bernat:"+Bernat); 
                }
                // Muestra quien ha ganado comprobando la puntuacion de Anna, si su puntuacion no es de 0, significa que gano y Bernat perdio, si es de 0, perdio.
                System.out.println((0<Anna)?"¡El ganador es Anna!":"¡El ganador es Bernat!");
                // Luego pregunta si se va a seguir con el juego, si se desea seguir podremos dar enter y seguir, si no, se escribe fin y la variable del bucle pasa a ser falsa y finaliza el
                System.out.println("¿Van a seguir jugando?");
                System.out.println("Para seguir en el juego, pulsa ENTER.");
                System.out.println("Para finalizar el juego, escribe fin.");
                String fin = buffer.readLine();
                if(fin.equalsIgnoreCase("fin")){
                    // Cambia bucle a falso, haciendo que el bucle se detenga.
                    bucle=false;
                }
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
