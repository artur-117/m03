package act_4;
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Akravchukpt4ej2 {
    public static void main(String[] args) {
                // Inicia un try con BufferredReader.
                try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
                    // Inicia las variables a y b.
                    int a = 0;
                    int b = 0;
                    // Pide introducir numeros para a y b, comprueba si son positivos, si no, vuelve a pedirlos.
                    System.out.println("Introduce el numero a");
                    while((a=Integer.parseInt(buffer.readLine()))<0)
                        System.out.println("Introduce el numero a");
                    System.out.println("Introduce B");
                    while((b=Integer.parseInt(buffer.readLine()))<0)
                        System.out.println("Introduce B");
                    // Bucle que multiplica la cantidad de veces necesaria.
                    for(int i = 1; i<b;i++){
                        a=a*b;
                    }
                    // Muestra el resultado.
                    System.out.println("El resultado es "+a);
                }catch(Exception e){
                    // Si llega aquí, significa que hubo un fallo.
                    e.printStackTrace();
                }
    }
}
