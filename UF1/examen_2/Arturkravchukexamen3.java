package examen_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arturkravchukexamen3 {
    public static void main(String[] args) {
                // Inicia un try con BufferredReader.
                try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
                    // Limpia la pantalla.
                    System.out.print("\033[H\033[J");
                    // Inicia suma y suma2, la suma sera la suma inicial y la suma 2 servira para comparar la siguiente suma con la inicial.
                    int suma = 0;
                    int suma2 = 0;
                    // Pedira introducir el numero candidato a ser centro numerico.
                    System.out.print("Introduce el numero centro numerico: ");
                    int cn = Integer.parseInt(buffer.readLine());
                    // Suma todos los numeros anteriores al numero introducido para el centro numerico.
                    for(int i = 0; i<cn; i++){
                        suma = suma+i;
                    }
                    // Suma los numeros siguientes al numero introducido, la suma se comparara con la anterior suma, si coinciden significa que es centro numerico.
                    // Si no coinciden y la suma2 ya es mayor a suma, significa que no puede ser centro numerico.
                    for(int j = cn+1; suma2!=suma; j++){
                        suma2=suma2+j;
                        // Si suma2 es mayor a suma, se detiene el bucle, no puede ser centro numerico.
                        if(suma<suma2){
                            break;
                        }
                    }
                    // Compara suma y suma2 para decir si es centro numerico o no.
                    System.out.println((suma==suma2)?cn+" es centro numerico.":cn+" no es centro numerico.");
                }catch(Exception e){
                    // Si llega aqui, hubo un fallo.
                    e.printStackTrace();
                }
    }
}
