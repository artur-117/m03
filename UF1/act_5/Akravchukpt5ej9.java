package act_5;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;


public class Akravchukpt5ej9 {
    public static void main(String[] args) {
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Inicia dos arrays de tipo string, para las palabras y luego para el orden.
            String[] palabras = new String[5];
            String[] orden = new String[5];
            // Inicia el menor con una string vacia.
            String menor = "";
            // Pide las 5 palabras.
            for(int i = 0; i<5;i++){
                System.out.print(i+": ");
                palabras[i] = buffer.readLine();
                System.out.println();
            }
            // Empieza a ordenar con este for, se repetira 5 veces el for.
            for(int i = 0; i<5;i++){
                // Dentro del mismo for, creamos otro for con la variable j que también se repitira 5 veces, ira comparando entre los 5 hasta saber exactamente que variable es menor.
                for(int j = 0; j<5;j++){
                    // Si la variable menor esta vacia, deja la primera variable como menor para empezar a comparar.
                    if(menor.isEmpty()){
                        menor=palabras[j];
                    }else{
                        // Si la variable menor no esta vacia, comprobara la primera palabra con las demás para detectar cual es la menor.
                        if(palabras[j].charAt(0)<menor.charAt(0)){
                            menor = palabras[j];
                        }
                    }
                }
                // Cada vez que sale de comprobar el menor, guarda la variable que quedo como menor en la array de orden con index de i.
                orden[i] = menor;
                // For que se repite 5 veces
                for(int d = 0; d<5;d++){
                    // Comprueba en cada palabra si es igual a menor, si lo es, re-escribe la palabra a }.
                    if(palabras[d].equals(menor)){
                        palabras[d] = "}";
                    }
                }
                // Vuelve a dejar menor vacia.
                menor = "";
            }
            // Muestra el resultado por pantalla.
            System.out.println("resultado: "+Arrays.toString(orden));
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}