// programa lleno de bugs, me di cuenta tarde que no tenia enunciado ☠

import java.util.ArrayList;
import java.util.Scanner;


//Este programa es un simple menu con 6 opciones, las 4 primeras son para hacer calculos, las 2 ultimas para visualizar los resultados, y para borrarlos.
public class Uf2Bug10{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // He importado arraylist y he añadido new ArrayList<Integer>();
        ArrayList<Integer> resultados = new ArrayList<Integer>();

        int opcion;
        do {
            System.out.println("╔═════════════════════════╗");
            System.out.println("║      MENU OPCIONES      ║");
            System.out.println("╠═════════════════════════╣");
            System.out.println("║ 1. Sumar                ║");
            System.out.println("║ 2. Restar               ║");
            System.out.println("║ 3. Multiplicar          ║");
            System.out.println("║ 4. Pon tu nombre        ║");
            System.out.println("║ 5. Mostrar Resultados   ║");
            System.out.println("║ 6. Borrar Resultado     ║");
            System.out.println("║ 0. Salir                ║");
            System.out.println("╚═════════════════════════╝");
            System.out.println("");
            System.out.print("- Introduce la opcion que deseas: ");
            opcion = scanner.nextInt();
            // he corregio opcion mal escrito
            switch (opcion) {
                case 1:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 1. Sumar                ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    System.out.print("- Ingrese el primer numero: ");
                    int num1 = scanner.nextInt();
                    System.out.println("");
                    System.out.print("- Ingrese el segundo numero: ");
                    int num2 = scanner.nextInt();
                    System.out.println("");
                    // he corregido la suma, antes restaba en vez de sumar
                    int suma = num1 + num2;
                    resultados.add(suma);
                    System.out.println("  La suma de " + num1 + " + " + num2  + " es: " + suma);
                    System.out.println("");
                    break;
                case 2:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 2. Restar               ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    System.out.print("- Ingrese el primer numero: ");
                    num1 = scanner.nextInt();
                    System.out.println("");
                    System.out.print("- Ingrese el segundo numero: ");
                    num2 = scanner.nextInt();
                    System.out.println("");
                    // he corregido la resta, antes multiplicaba en vez de restar
                    int resta = num1 - num2;
                    resultados.add(resta);
                    System.out.println("  La resta de " + num1 + " - " + num2  + " es: " + resta);
                    System.out.println("");
                    break;
                case 3:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 3. Multiplicar          ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    System.out.print("- Ingrese el primer numero: ");
                    num1 = scanner.nextInt();
                    System.out.println("");
                    System.out.print("- Ingrese el segundo numero: ");
                    num2 = scanner.nextInt();
                    System.out.println("");
                    // he corregido la multiplicacion, antes dividia en vez de multiplicar
                    int multiplicacion = num2 * num1;
                    resultados.add(multiplicacion);
                    System.out.println("  La multiplicacion de " + num1 + " * " + num2  + " es: " + multiplicacion);
                    System.out.println("");
                    break;
                case 4:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 4. Pon tu nombre        ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    System.out.print("- Ingrese su nombre para que se imprimia por pantalla: ");
                    String nombre = scanner.next();
                    // he quitado que se ponga Julio después de escribir mi nombre
                    //System.out.println("");
                    //nombre = "Julio";
                    System.out.println("  Hola " + nombre);
                    System.out.println("");
                    break;
                case 5:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 5. Mostrar Resultados   ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    for (int resultado : resultados) {
                        System.out.println("  " + resultado);
                        System.out.println("");
                    }
                    break;
                case 6:
                    System.out.println("");
                    System.out.println("╔═════════════════════════╗");
                    System.out.println("║ 6. Borrar Resultado     ║");
                    System.out.println("╚═════════════════════════╝");
                    System.out.println("");
                    System.out.print("- Ingrese el numero a borrar: ");
                    int numborrar = scanner.nextInt();
                    System.out.println("");
                    
                    // añadir comprobacion si existe el numero y cual es para borrarlo.
                    Boolean borrado = false;
                    for(int i = 0; i<resultados.size();i++){
                        if(numborrar==resultados.get(i)){
                            resultados.remove(i);
                            System.out.println("  ¡Numero borrado correctamente!");
                            System.out.println("");
                            borrado = true;
                        }
                    }
                    if(!borrado){
                        System.out.println("  ¡El numero introducido no existe!");
                        System.out.println("");

                    }
                    break;
                case 0:
                    System.out.println("");
                    System.out.println("  ¡Has salido del programa!");
                    break;
                default:
                    System.out.println("");
                    System.out.println("  ¡Opcion no valida!");
                    System.out.println("");
            }
        } while (opcion != 0);
    }
}
