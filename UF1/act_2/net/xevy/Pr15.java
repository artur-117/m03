import java.util.Scanner;

public class Pr15 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // Guarda los doubles obtenidos en variables. numero horas y precio hora.
        double numhoras = getd();
        double ph = getd();
        // Comprueba si hizo menos de 35 horas, si las hizo hacer el calculo sin tomar en cuenta horas extra, si hizo más de 35 horas tomar horas extra en cuenta.
        if(35<numhoras){
            double horas_extra = numhoras-35;
            double horas = 35;
            double precioextras = ph+(0.5*ph);
            double pex = horas_extra*precioextras;
            double p = horas*ph;
            double pt = p+pex;
            System.out.println("Hay horas extra, debes pagarle en total: "+pt+"€");
        }else{
            Double p = numhoras*ph;
            System.out.println("No hay horas extra, debes pagarle en total: "+p+"€");
        }
    }
    public static double getd(){
        // Lee el numero double que se envie por teclado.
        double d = scanner.nextDouble();
        return d;
    }

}
