import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Dibuja en pantalla una figura geométrica según criterios elegidos por el
 * usuario. Primero se pregunta la figura (cuadrado o triángulo). Luego su
 * tamaño (3-30), si es hueca y finalmente el color. En caso de introducir un
 * color desconocido, el programa utilizará uno aleatorio.
 * 
 * BUG Línea x: x
 * BUG Línea x: x
 * 
 */
public class Uf2Bug2 {
	static final String[] COLORES = { "\033[31m", "\033[32m", "\033[33m", "\033[34m" };

	public static int nombreAColor(String nombre) {
		switch (nombre.toLowerCase().trim()) {
			case "rojo":
				return 0;
			case "verde":
				return 1;
			case "amarillo":
				return 2;
			case "azul":
				return 3;
			default:
				System.out.println("Color desconocido, un color aleario sera utilitzado");
				return new Random().nextInt(COLORES.length);
		}
	}

	public static void hacerCuadrado(int medida, boolean hueco) {
		for (int i = 0; i < medida; i++) {
			// he cambiado i a j.
			for (int j = 0; j < medida; j++) {
				if (!hueco || (i == 0 || i == medida - 1 || j == 0 || j == medida - 1)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}

			System.out.println();
		}
	}

	public static void hacerTriangulo(int altura, boolean hueco) {
		for (int i = 0; i < altura; i++) {
			if (hueco) {
				for (int j = 0; j < altura - i - 1; j++) {
					System.out.print(" ");
				}

				System.out.print("*");

				if (i == altura - 1) {
					for (int k = 0; k < i - 1; k++)
						System.out.print(" *");
				} else {
					for (int k = 0; k < 2 * i - 1; k++) {
						System.out.print(" ");
					}
				}

				if (i == altura - 1) {
					System.out.println(" *");
				} else if (i != 0) {
					System.out.print("*");
				}
			} else {
				for (int j = 0; j < altura - (i + 1); j++) {
					System.out.print(" ");
				}

				for (int j = 0; j < (i + 1) * 2 - 1; j++) {
					System.out.print("*");
				}
			}

			System.out.println();
		}
	}

	public static void main(String[] args) {
		System.out.println("\033[34mGenerador de Figuras Geométricas\033[0m\n");

		try (BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))) {
			boolean continuar = true;

			
			while (continuar) {
				String figura = "";

					System.out.print("Que figura quieres? (Cuadrado/Triangulo) ");
					figura = buffer.readLine().trim().toLowerCase();
					// quite este bucle infinito
					//continuar = figura.equals("cuadrado") && figura.equals("triangulo");
					//
					//if (!continuar)
					//System.out.println("Esa figura no esta disponible!");
					//} while (!continuar);



				int medida = 3;

				do {
					System.out.print("De que tamaño (3-30)? ");

					try {
						medida = Integer.parseInt(buffer.readLine());
						continuar = medida >= 3 && medida <= 30;

						if (!continuar) {
							System.out.println("Tamaño fuera del limite!");
						}
					} catch (NumberFormatException e) {
						continuar = false;
						System.out.println("Eso no es un numero!");
					}
				} while (!continuar);

				System.out.print("Figura Hueca? (s/N) ");
				boolean hueco = buffer.readLine().trim().equalsIgnoreCase("s");

				System.out.print("De que color? (rojo, verde, amarillo o azul) ");
				int color = nombreAColor(buffer.readLine());

				System.out.println("\nFigura generada:\n" + COLORES[color]);

				switch (figura) {
					case "cuadrado":
						hacerCuadrado(medida, hueco);
						break;
					case "triangulo":
						hacerTriangulo(medida, hueco);
				}

				System.out.print("\n\033[0mDeseas generar otra figura? (S/n) ");
				continuar = !buffer.readLine().trim().equalsIgnoreCase("n");
			}
		} catch (Exception e) {
			System.out.println("Ha ocurrido un error al ejecutar el programa");
			e.printStackTrace();
		}
	}
}
