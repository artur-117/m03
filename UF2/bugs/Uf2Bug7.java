// Este programa es una calculadora pedira dos numeros y operacion , antes de empezar el programa pedira las veces que querras hacer las operaciones
// Al final de cada operacion pedira si el usuario quiere seguir o no quiere seguir si pone s minuscula seguira pidiendo numeros si lo pone en mayuscula el programa le debe informar que tiene que ser en minuscula
// En el momento en el que se llega al màximo de operaciones que has fijado el usuario el programa no te permitira seguir y cerrara
// el programa si haces una division se quejara si el numero2 es un 0
// en caso de no poner una operacion valida el programa tambien se quejara.
import java.util.Scanner;
/*los while continuan mientras el usuario diga diferente a s y a n

        do {
            if (contador == maxOper){
                System.out.println("Has llegado a las operaciones que has querido");
                break;
            }
            char operacion;
            do {
                System.out.print("Introduce el primer número: ");
                double num1 = scanner.nextDouble();
                
                System.out.print("Introduce el tipo de operación (+, -, *, /): ");
                operacion = scanner.next().charAt(0);

                System.out.print("Introduce el segundo número: ");
                double num2 = scanner.nextDouble();

                resultado = calcular(num1, operacion, num2);
                System.out.println("El resultado es: " + resultado);
            } while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');
            contador ++;         
            do {
                
                System.out.print("¿Quieres hacer otra operación? (s/n): ");
                continuar = scanner.next().charAt(0);

                if (continuar == 'S' && continuar == 'N'){
                    System.out.println("ES EN MINUSCULAS");
                }
            
            } while (continuar != 's' && continuar != 'n');
                
        } while (continuar != 's');
 * 
 * 
 * se cambiaron por 
 * 
 *             } while (!String.valueOf(continuar).equalsIgnoreCase("s") && !String.valueOf(continuar).equalsIgnoreCase("n"));
                
        } while (String.valueOf(continuar).equalsIgnoreCase("s"));



        el problama se queja si es en mayuscula el salir
        --->
                        if (String.valueOf(continuar).equals("S") && !String.valueOf(continuar).equals("N")){
                    System.out.println("ES EN MINUSCULAS");
                }


        --> problemas al poner string solucion : valor string se pasara si solo es digito
                do{
            valor=scanner.nextLine();
        }while(!valor.matches("[0-9]+"));
        int maxOper = Integer.parseInt(valor);


        añadido try catch para que se queje

                    try{
                double num1 = scanner.nextDouble();
                
                System.out.print("Introduce el tipo de operación (+, -, *, /): ");
                operacion = scanner.next().charAt(0);

                System.out.print("Introduce el segundo número: ");
                double num2 = scanner.nextDouble();

                resultado = calcular(num1, operacion, num2);
                
                System.out.println("El resultado es: " + resultado);
            }catch(Exception e){
                // se quejo
                System.out.println("Me quejo");
                operacion='+';
            }
            } while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');
 */
public class Uf2Bug7 {

    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        char continuar;
        double resultado = 0;
        System.out.println("Cuantas operaciones quieres hacer? ");
        int maxOper = scanner.nextInt();
        int contador = 0;
        do {
            if (contador == maxOper){
                System.out.println("Has llegado a las operaciones que has querido");
                break;
            }
            char operacion;
            do {
                System.out.print("Introduce el primer número: ");
                double num1 = scanner.nextDouble();
                
                System.out.print("Introduce el tipo de operación (+, -, *, /): ");
                operacion = scanner.next().charAt(0);

                System.out.print("Introduce el segundo número: ");
                double num2 = scanner.nextDouble();

                resultado = calcular(num1, operacion, num2);
                System.out.println("El resultado es: " + resultado);
            } while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');
            contador ++;         
            do {
                
                System.out.print("¿Quieres hacer otra operación? (s/n): ");
                continuar = scanner.next().charAt(0);

                if (continuar == 'S' && continuar == 'N'){
                    System.out.println("ES EN MINUSCULAS");
                }
            
            } while (continuar != 's' && continuar != 'n');
                
        } while (continuar != 's');

        System.out.println("¡Hasta luego!");
        scanner.close();
    }

    public static double calcular(double num1, char operacion, double num2) {
        switch (operacion) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '*':
                return num1 * num2;
            case '/':
                if (num2 != 0) {
                    return num1 / num2;
                } else {
                    System.out.println("No se puede dividir entre cero. Introduce otro segundo número.");
                    return Double.NaN; 
                }
            default:
                System.out.println("Operación no válida.");
                return Double.NaN; 
        }
    }
}
