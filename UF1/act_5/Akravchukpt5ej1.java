package act_5;

import java.util.Random;

public class Akravchukpt5ej1 {
    public static void main(String[] args) {
        // Inicia un try.
        try{
            // Inicia una variable aleatoria, la usaremos para los dados.
            Random aleatorio = new Random();
            // Inicia las variables de ambos dados.
            int dado = 0;
            int dado2 = 0;
            // Calcula y muestra los dados caidos, sera n numero aleatorio del 1 al 6.
            System.out.println("Los dados caidos son: "+(((dado=aleatorio.nextInt(7))==0)?1:dado)+" y "+(((dado2=aleatorio.nextInt(7))==0)?1:dado2));
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
