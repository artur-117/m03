import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt3ej6 {
    public static void main(String[] args) {
        System.out.println("¿Cuantos numeros introduciras?");
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Lee en n la cantidad de numeros que se introduciran.
            int n = Integer.parseInt(buffer.readLine());
            // Crea una array con la cantidad de numeros a introducir.
            int[] na = new int[n];
            // Bucle para pedirle los numeros a introducir.
            for(int i = 0; i<n; i++){
                // Guarda en cada posicion de la array un numero.
                System.out.print("Introduce el siguiente numero: ");
                na[i] = Integer.parseInt(buffer.readLine());
            }

            // Guarda la longitud de la array.
            int nalen = na.length;
            // Inicia menor y mayor.
            int menor = 0;
            int mayor = 0;
            // Bucle en el que se le asignaran los numeros a menor o mayor dependiendo si son mayores o menores.
            for(int x = 0; x<nalen;x++){
                if(mayor==0&&menor==0){
                    menor = na[x];
                    mayor = na[x];
                }else{
                    System.out.println(na[x]);
                    if(mayor<na[x]){
                        mayor = na[x];
                    }
                    if(na[x]<menor){
                        menor = na[x];
                    }
                }
            }
            // Calculo para la suma total de todos los numeros.
            int suma = 0;
            for(int f = 0; f<nalen; f++){
                suma = suma + na[f];
            }
            // Muestra la media, el mayor y el menor de todos los numeros.
            System.out.println("Media: "+Double.parseDouble(Integer.toString(suma))/Double.parseDouble(Integer.toString(nalen)));
            System.out.println("Mayor: "+mayor+" y menor: "+menor);
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
