// Este programa es una calculadora pedira dos numeros y operacion , antes de empezar el programa pedira las veces que querras hacer las operaciones
// Al final de cada operacion pedira si el usuario quiere seguir o no quiere seguir si pone s minuscula seguira pidiendo numeros si lo pone en mayuscula el programa 
//le debe informar que tiene que ser en minuscula
// En el momento en el que se llega al màximo de operaciones que has fijado el usuario el programa no te permitira seguir y cerrara
// el programa si haces una division se quejara si el numero2 es un 0
// en caso de no poner una operacion valida el programa tambien se quejara.
/*  solucion

los while continuan mientras el usuario diga diferente a s y a n

        do {
            if (contador == maxOper){
                System.out.println("Has llegado a las operaciones que has querido");
                break;
            }
            char operacion;
            do {
                System.out.print("Introduce el primer número: ");
                double num1 = scanner.nextDouble();
                
                System.out.print("Introduce el tipo de operación (+, -, *, /): ");
                operacion = scanner.next().charAt(0);

                System.out.print("Introduce el segundo número: ");
                double num2 = scanner.nextDouble();

                resultado = calcular(num1, operacion, num2);
                System.out.println("El resultado es: " + resultado);
            } while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');
            contador ++;         
            do {
                
                System.out.print("¿Quieres hacer otra operación? (s/n): ");
                continuar = scanner.next().charAt(0);

                if (continuar == 'S' && continuar == 'N'){
                    System.out.println("ES EN MINUSCULAS");
                }
            
            } while (continuar != 's' && continuar != 'n');
                
        } while (continuar != 's');
 * 
 * 
 * se cambiaron por 
 * 
 *             } while (!String.valueOf(continuar).equalsIgnoreCase("s") && !String.valueOf(continuar).equalsIgnoreCase("n"));
                
        } while (String.valueOf(continuar).equalsIgnoreCase("s"));



        el problama se queja si es en mayuscula el salir
        --->
                        if (String.valueOf(continuar).equals("S") && !String.valueOf(continuar).equals("N")){
                    System.out.println("ES EN MINUSCULAS");
                }


        --> problemas al poner string solucion : valor string se pasara si solo es digito
                do{
            valor=scanner.nextLine();
        }while(!valor.matches("[0-9]+"));
        int maxOper = Integer.parseInt(valor);


        añadido try catch para que se queje

                    try{
                double num1 = scanner.nextDouble();
                
                System.out.print("Introduce el tipo de operación (+, -, *, /): ");
                operacion = scanner.next().charAt(0);

                System.out.print("Introduce el segundo número: ");
                double num2 = scanner.nextDouble();

                resultado = calcular(num1, operacion, num2);
                
                System.out.println("El resultado es: " + resultado);
            }catch(Exception e){
                // se quejo
                System.out.println("Me quejo");
                operacion='+';
            }
            } while (operacion != '+' && operacion != '-' && operacion != '*' && operacion != '/');
 */
import java.util.Random;
import java.util.Scanner;

public class Uf2Bug5 {

    // Variable global para almacenar la puntuación del jugador
    public static int punts = 0;

    public static void main(String[] args) {
        // Crear objetos Scanner y Random para entrada y generación de números aleatorios
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        // Mensaje de bienvenida
        System.out.println("Benvingut al joc d'operacions!");

        // Bucle principal del juego
        while (true) {
            // Generar tres números aleatorios entre 1 y 10
            int num1 = random.nextInt(10) + 1;
            int num2 = random.nextInt(10) + 1;
            int num3 = random.nextInt(10) + 1;


            // Generar dos operaciones aleatorias entre suma (+), resta (-) y multiplicación (*)
            String operacio1 = generarOperacio();
            String operacio2 = generarOperacio();


            // Mostrar las operaciones al jugador
            System.out.println("Calcula el resultat de les operacions següents:");
            System.out.println(num1 + " " + operacio2 + " " + num2 + " " + operacio1 + " " + num3);

            // Obtener las respuestas del jugador
            int resposta1 = scanner.nextInt();


            // Comprobar las respuestas y actualizar la puntuación
            if (comprovarResultat(resposta1, num1, num2, num3, operacio1, operacio2)) {
                punts++;
                System.out.println("Encertaste! Tens " + punts + " punts.");
            } else {
                // Mostrar mensaje de game over y puntuación final
                System.out.println("Game over! Tens " + punts + " punts.");
                break;
            }
        }
    }

    // Método para generar una operación aleatoria entre suma, resta y multiplicación
    public static String generarOperacio() {
        Random random = new Random();
        int operacio = random.nextInt(3);
        int operacio2 = random.nextInt(3);

        switch (operacio) {
            case 0:
                return "+";
            case 1:
                return "-";
            case 2:
                return "*";
        }
        switch (operacio2) {
            case 0:
                return "+";
            case 1:
                return "-";
            case 2:
                return "*";
            default:
                return "+";
        }
    }

    // Método para comprobar si la respuesta del jugador es correcta
    public static boolean comprovarResultat(int resposta, int num1, int num2, int num3, String operacio, String operacio2) {
        int resultat;
        int resultat1;

        // Calcular el resultado según la operación seleccionada
        switch (operacio) {
            case "+":
                resultat1 = num1 + num2;
                break;
            case "-":
                resultat1 = num1 - num2;
                break;
            case "*":
                resultat1 = num1 * num2;
                break;
            default:
                resultat1 = num1 + num2;
                break;
        }
        switch (operacio2) {
            case "+":
                resultat = resultat1 + num3;
                break;
            case "-":
                resultat = resultat1 - num3;
                break;
            case "*":
                resultat = resultat1 * num3;
                break;
            default:
                resultat = resultat1 + num3;
                break;
        }

        // Comparar la respuesta del jugador con el resultado calculado
        return resposta == resultat;
    }
}
