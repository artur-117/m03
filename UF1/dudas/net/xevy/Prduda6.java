import java.util.Scanner;

public class Prduda6 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
            int num1 = scanner.nextInt();
            int num2 = scanner.nextInt();

            //Condiciones que comprueban si son menores, si no lo son, significa que son iguales por lo cual responde que son iguales.
            if(num1<num2){
                System.out.println(num1+" es menor a "+num2);
            }else if(num2<num1){
                System.out.println(num2+" es menor a "+num1);
            }else{
                System.out.println("Los numeros son iguales.");
            }
    }
}
