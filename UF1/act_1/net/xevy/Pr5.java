import java.util.Scanner;

public final class Prd5 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try{
			// Llama a la función para obtener celsius y mostrarlo por pantalla.
            double c = getcelsius();
            System.out.println(c);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static double getcelsius(){
		// Obtiene los celsius convirtiendo fahrenheit a celsius con su formula.
        double f = getd();
        double c = (f-32)*5/9;
        return c;
    }

    public static double getd(){
		// Obtiene un numero double.
        double d = scanner.nextDouble();
        return d;
    }

}
