package act_5;
public class Akravchukpt5ej5 {
    public static void main(String[] args) {
        // Establece el texto principal.
        String texto = "La Casa Azul";
        // Separa el texto por espacios y lo guarda en un array.
        String[] textoseparado = texto.split(" ");
        // Recorre la array creada y muestra el texto separado por pantalla.
        for(int i = 0; i<textoseparado.length; i++){
            System.out.println(textoseparado[i]);
        }
    }
}