import java.util.Scanner;

public class Pr13b {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        // Guarda todos los numeros obtenidos en variables.
        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();
        int n3 = scanner.nextInt();
        // Comprueba si esta bien el orden y guarda un verdadero o falso en caso de que este bien o mal el orden.
        Boolean orden = (n1<n2 && n2<n3)?true:false; 
        System.out.println(orden);
    }
}
