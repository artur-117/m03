package act_4;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukpt4ej3 {
    public static void main(String[] args) {
                // Inicia un try con BufferredReader.
                try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
                    System.out.println("OBTENER VALOR (1)");
                    System.out.print("Numero de jugadores: ");
                    // Inicia los jugadores y la variable para los Randoms.
                    int jugadores = 0;
                    Random aleatorio = new Random();
                    // Bucle que comprueba si los jugadores son positivos, si son negativos sera imposible avanzar y terminara el programa.
                    if(0<(jugadores = Integer.parseInt(buffer.readLine()))){
                        // Pide el valor a conseguir, debe estar entre 1 y 6 según el siguiente if..
                        System.out.print("Valor a conseguir: ");
                        int valor = Integer.parseInt(buffer.readLine()); 
                        if(0<valor&&valor<7){
                            // Bucle que genera los aleatorios por cada jugador y comprueba si se ha conseguido el valor deseado o no.
                            for(int i = 1; i<=jugadores; i++){
                                int al = ((al=aleatorio.nextInt(7))==0)?1:al;
                                System.out.println((valor==al)?"Jugador "+i+": "+al+" CONSEGUIDO":"Jugador "+i+": "+al);
                            }
                        }else{
                            // Si el valor deseado no es entre 1 y 6, sera imposible ya que el dado tiene numeros entre el 1 y 6.
                            System.out.println("¡Imposible conseguir un "+valor+"!");
                        }
                    }else{
                        // Imposible si los jugadores son negativos.
                        System.out.println("¡IMPOSIBLE!");
                    }
                }catch (Exception e) {
                    // Si llega aquí, significa que hubo un fallo.
                    e.printStackTrace();
                }
    }
}
