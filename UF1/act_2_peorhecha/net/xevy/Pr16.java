import java.util.Scanner;


public class Pr16 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        //Guarda las opciones del menu en array.
        System.out.println("=======================================");
        String[] menu = new String[6];
        menu[0] = "= 1.-Euros a Dolares";
        menu[1] = "= 2.-Euros a Yenes";
        menu[2] = "= 3.-Dolares a Euros";
        menu[3] = "= 4.-Dolares a Yenes";
        menu[4] = "= 5.-Yenes a Euros";
        menu[5] = "= 6.-Yenes a Dolares";
        // Muestra el menu.
        int ml = menu.length;
        for(int i= 0;i<ml;i++){
            System.out.println(menu[i]);
        }
        System.out.println("=======================================");
        System.out.println("= Selecciona una opción de conversión =");
        try{
            // Guarda el numero entero en una variable para la opción.
            int opcion = scanner.nextInt();
            System.out.println("=======================================");
            System.out.println("= Selecciona una cantidad de conversión =");
            // Guarda el numero double en variable de moneda, esta sera convertida.            
            double monedas = scanner.nextDouble();
            // Dependiendo de la opcion, realiza el calcula para convertir de una a otra moneda.
            switch(opcion){
                case 1:
                    System.out.println("Euros a Dolares");

                    System.out.println(monedas*1.11894);
                    break;
                case 2:
                    System.out.println("Euros a Yenes");
                    System.out.println(monedas*115.998);
                    break;

                case 3:
                    System.out.println("Dolares a Euros");
                    System.out.println(monedas/1.11894);
                    break;
                case 4:
                    System.out.println("Dolares a yenes");
                    double dolconveuro = monedas/1.11894;
                    System.out.println(dolconveuro*115.998);
                    break;
                case 5:
                    System.out.println("Yenes a Euros");
                    System.out.println(monedas/115.998);
                    break;
                case 6:
                    System.out.println("Yenes a Dolares");
                    double yenconveuro = monedas/115.998;
                    System.out.println(yenconveuro*1.11894);
                    break;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
