import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Pr2 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try{
            System.out.println();
            System.out.println("Nombre: ");
            String nombre = getn();
            System.out.println("Tarea: ");
            String tarea = getn();
            SimpleDateFormat formato = new SimpleDateFormat("hh:mm:ss"); 
            String hora = formato.format(new Date(System.currentTimeMillis()));
            System.out.println(nombre+" ha realizado la tarea: "+tarea+" a las "+hora);
    
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getn(){
        String n = scanner.nextLine();
        return n;
    }
}
