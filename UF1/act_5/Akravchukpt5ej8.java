package act_5;

public class Akravchukpt5ej8 {
    public static void main(String[] args) {
        // Inicia una variable Dupla a partir de la clase creada.
        Dupla test = new Dupla();
        // Inicia una array de tipo Dupla de 10.
        Dupla[] lista = new Dupla[10];
        // Inicia la variable txi con 97, este numero representa la "a", luego se ira sumando para aumentar.
        int txi = 97;
        // Un for que se repite 10 veces para generar la lista.
        for(int i = 0; i<10; i++){
            // Dejamos el numero y texto de la variable test en la i, en el caso del texto se suma la i.
            test.numero = i;
            test.texto = String.valueOf((char) (txi+i));
            // En el index de la i para la array de tipo dupla, almacenamos la dupla generada en test.
            lista[i] = test;
            // Mostramos por pantalla.
            System.out.println(lista[i].numero+","+lista[i].texto);
        }
    }
}