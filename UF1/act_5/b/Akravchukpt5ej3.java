package act_5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;


public class Akravchukpt5ej3 {
    public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) {
        try{
            System.out.println(validacion_nombre());
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }

    public static boolean validacion_nombre() throws IOException {
        Boolean nc = true;
        String nombre = buffer.readLine();
        if(6<=nombre.length()&&nombre.length()<=12){
            for(int i = 0; i<nombre.length();i++){
                if(nombre.charAt(i) >= '0' & nombre.charAt(i)<='9'||nombre.charAt(i)>='a' && nombre.charAt(i)<='z'||nombre.charAt(i)>='A' && nombre.charAt(i)<='Z'){
                }else{
                    nc = false;
                    System.out.println("El nombre debe contener solo letras y numeros.");
                    break;
                }
            }
            if(nc){
                JOptionPane.showMessageDialog(null, "Tu nombre es "+nombre, nombre, JOptionPane.INFORMATION_MESSAGE);
                return true;
            }else{
                return false;
            }
        }else{
            if(nombre.length()<6){
                System.out.println("El nombre debe contener almenos 6 caracteres.");
            }else{
                System.out.println("El nombre debe contener como maximo 12 caracteres.");
            }
            return false;
        }
    }
}