package examen_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Arturkravchukexamen1 {
    public static void main(String[] args) {
                // Inicia un try con BufferredReader.
                try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
                    // Inicia un booleano con true para usarlo en el bucle.
                    boolean bucle = true;
                    // Inicia las variables de kilometros y toneladas.
                    int km = 0;
                    int t = 0;
                    while(bucle){
                        // Limpia la pantalla.
                        System.out.print("\033[H\033[J");

                        // Muestra el menu por pantalla.
                        System.out.println("1 - Avioneta");
                        System.out.println("2 - Airbus");
                        System.out.println("3 - Boing");
                        System.out.println("4 - Antonov");
                        System.out.println("5 - Salir");
                        // Pide que se seleccione una opción
                        System.out.print("Seleccione la opción deseada: ");
                        int opcion = Integer.parseInt(buffer.readLine());
                        // Pide los kilometros y las toneladas verificando que la opción seleccionada no sea 5.
                        if(opcion!=5){
                            System.out.print("Kilómetros y toneladas: ");
                            String[] kmt = buffer.readLine().split(" ");
                            km = Integer.parseInt(kmt[0]);
                            t = Integer.parseInt(kmt[1]);
                        }
                        // Switch con los distintos casos para cada opción.
                        switch(opcion){
                            // Si es una avioneta, el importe fijo es de 500.
                            case 1:
                                System.out.println("Importe = 500");
                                System.out.println("");
                                System.out.println("Enter para continuar.");
                                buffer.readLine();
                                break;
                            // Si es un Airbus el importe es de 60 por cada kilometro.
                            case 2:
                                System.out.println("Importe = "+(60*km));
                                System.out.println("");
                                System.out.println("Enter para continuar.");
                                buffer.readLine();
                                break;
                            // Si es un Boing el importe es de 60 por cada kilometro.
                            case 3:
                                System.out.println("Importe = "+(60*km));
                                System.out.println("");
                                System.out.println("Enter para continuar.");
                                buffer.readLine();
                                break;
                            // Si es un Antonov el importe es de 60 por cada kilometro + 50 por cada tonelada.
                            case 4:
                                System.out.println("Importe = "+((60*km)+(50*t)));
                                System.out.println("");
                                System.out.println("Enter para continuar.");
                                buffer.readLine();
                                break;
                            // Aqui saldra del programa.
                            case 5:
                                bucle = false;
                                System.out.println("Saliste del programa.");
                                break;
                            // Se dio una opción incorrecta, pedira repetir.
                            default:
                                System.out.println("Parece que hubo un error...");
                                System.out.println("¡Vuelve a seleccionar una opción!");
                                System.out.println("");
                                System.out.println("Enter para continuar.");
                                buffer.readLine();
                                break;
                        }
                    }
                }catch(Exception e){
                    // Si llega aquí, hubo un fallo.
                    e.printStackTrace();
                }
    }
}
