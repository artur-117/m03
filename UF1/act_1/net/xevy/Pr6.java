import java.util.Scanner;
public class Pr6 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try{
			// Llama a la función getresults, esta función ya pedira dos numeros y devolvera 
			// todos los calculos realizados para mostrar por pantalla.
            int[] resultados = getresults();
            int l = resultados.length;
            for(int i = 0;i<l;i++){
                System.out.println(resultados[i]);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static int[] getresults(){
		// Pide dos numeros y a partir de ellos realiza sumas,restas,multiplicaciones y divisiones, los 
		// guarda en una array.
        int num1 = geti();
        int num2 = geti();
        int[] resultado = new int[4];
        resultado[0] = num1+num2;
        resultado[1] = num1-num2;
        resultado[2] = num1*num2;
        resultado[3] = num1/num2;
        return resultado;
    }
    public static int geti(){
		// Obtiene un numero entero.
        int i = scanner.nextInt();
        return i;
    }
}
