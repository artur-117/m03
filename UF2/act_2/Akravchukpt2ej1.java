package UF2.act_2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;




public class Akravchukpt2ej1 {
    public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    public static String operadores[] = {"+","-","*"};

    public static void main(String[] args) {
        try{
            Random random = new Random();
            int a = random.nextInt(9);
            int b = random.nextInt(9);
            int c = random.nextInt(9);
            int op = random.nextInt(3);
            int op2 = 2;
            System.out.println(a+operadores[op]+b+operadores[op2]+c);
            if(op2==2){
                int res1 = operacion(b, c, op2);
                int res2 = operacion(a, res1, op);
                System.out.println(res2);
            }else{
                int res1 = operacion(a, b, op);
                int res2 = operacion(res1, c, op2);
                System.out.println(res2);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }
    


    public static int operacion(int a, int b, int op){
        switch(op){
            case 0:
                return a+b;
            case 1:
                return a-b;
            case 2:
                return a*b;
            default:
                return 0;
        }
    }

    public static int geti() throws NumberFormatException, IOException{
		// Lee lineas.
        int i = Integer.parseInt(buffer.readLine());
        return i;
    }
}