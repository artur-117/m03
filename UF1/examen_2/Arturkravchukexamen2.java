package examen_2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Arturkravchukexamen2 {
    public static void main(String[] args) {
                // Inicia un try con BufferredReader.
                try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
                    // Limpia la pantalla.
                    System.out.print("\033[H\033[J");
                    
                    // Establecemos el formato en 0.#, lo que significa que si el resultado es 5.0 se mostrara como 5 sin el .0, si es 5.5, se muestra normal como 5.5.
                    DecimalFormat format = new DecimalFormat("0.#");

                    // Inicia un booleano con true para usarlo en el bucle.
                    boolean bucle = true;
                    // Iniciamos el numero de batallas en 1 y los asesinatos a 0 todos.
                    int nb = 1;
                    Double asesinatos_m = 0.0;
                    Double asesinatos_g = 0.0;
                    // Muestra unos mensajes sobre el juego antes de iniciarlo.
                    System.out.println("||| Reglas de Juego. |||");
                    System.out.println("En cada batalla podras seleccionar el numero de asesinatos que ha obtenido cada personaje.");
                    System.out.println("Una vez que quieras detener las batallas debes de escribir fin.");
                    // Al pulsar enter pasara al bucle en donde se limpiara inicialmente la pantalla.
                    System.out.println("Para empezar pulsa ENTER");
                    buffer.readLine();
                    // Bucle que se detendra una vez que la variable bucle sea falsa.
                    while(bucle){
                        // Limpia la pantalla.
                        System.out.print("\033[H\033[J");
                        // Muestra el numero de la batalla.
                        System.out.println("[Batalla "+nb+"]");
                        // Pide el numero de orcos que ha asesinado el Mago en esta batalla.
                        System.out.print("Numero de orcos que ha asesinado el Mago: ");
                        asesinatos_m = asesinatos_m+Integer.parseInt(buffer.readLine());
                        // Pide el numero de orcos que ha asesinado el Guerrero en esta batalla.
                        System.out.print("Numero de orcos que ha asesinado el Guerrero: ");
                        asesinatos_g = asesinatos_g+Integer.parseInt(buffer.readLine());
                        // Pide pulsar ENTER para avanzar a la siguiente batalla o escribir fin para finalizar el juego y ver las estadisticas.
                        System.out.println("Pulsa ENTER para pasar a la siguiente batalla o escribe fin para detenerte: ");
                        String fin = buffer.readLine();
                        if(fin.equalsIgnoreCase("fin")){
                            // Cambia bucle a falso, haciendo que el bucle se detenga.
                            bucle=false;
                        }else{
                            // Aumenta en 1 el numero de batallas.
                            nb++;
                        }
                    }
                    // Limpia la pantalla.
                    System.out.print("\033[H\033[J");
                    // Muestra la estadistica del numero de batallas.
                    System.out.println("Ha habido un total de "+nb+" batallas.");
                    // Muestra los asesinatos totales de ambos personajes junto al porcentaje, este porcentaje se calcula sobre el numero total de asesinatos sumando los asesinatos de ambos.
                    System.out.println("Asesinatos del mago: "+asesinatos_m+" lo que se traduce en "+format.format(((asesinatos_m*100)/(asesinatos_g+asesinatos_m)))+"% de las muertes totales.");
                    System.out.println("Asesinatos del guerrero: "+asesinatos_g+" lo que se traduce en "+format.format(((asesinatos_g*100)/(asesinatos_g+asesinatos_m)))+"% de las muertes totales.");
                }catch(Exception e){
                    // Si llega aqui, hubo un fallo.
                    e.printStackTrace();
                }
    }
}
