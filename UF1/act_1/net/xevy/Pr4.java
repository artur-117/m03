import java.util.Scanner;

public final class Pr4 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try{
			// Llama a la función dinero, esta función mostrara por pantalla el dinero total a pagar.
            double dinero = dineros();
            System.out.println(dinero);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static double dineros(){
		// Multiplica el dinero por el interes y devuelve el resultado, el interes viene de otra función más abajo.
        System.out.println("Cantidad de dinero");
        double d = getd();
        double resultado = d*interes();
        return resultado;
    }

    public static double interes(){
		// Calcula el interes según la tasa y según los años.
        System.out.println("Tasa de interes");
        double ti = getd();
        System.out.println("Year");
        double year = getd();
        double resti = 1+ti/100;
        double restiyear = 0;
        for(int i = 0; i<year-1; i++){
            if(i==0){
                restiyear=resti*resti;
            }else{
                restiyear*=resti;
            }
        }
        return restiyear;
    }

    public static double getd(){
		// Obtiene un numero double.
        double d = scanner.nextDouble();
        return d;
    }

}
