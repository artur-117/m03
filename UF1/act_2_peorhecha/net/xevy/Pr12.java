import java.util.Scanner;


public class Pr12 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try{
            double preciohora = 30;
            double preciometro = 0.5;
            
            // Obtiene los valores tanto para horas trabajadas como para metros instalados.
            System.out.println("Horas trabajadas:");
            double h = scanner.nextDouble();
            System.out.println("Metros instalados:");
            double nm = scanner.nextDouble();
    
            // Calcula el total y luego calcula su iva.
            double total = preciohora*h+preciometro*nm;
            double totaliva = total*1.21;
            System.out.println("Has ganado "+total+" € en bruto.");
            System.out.println("Has ganado "+totaliva+" € con iva.");
        }catch(Exception e){
            e.printStackTrace();
        }
   }
}
