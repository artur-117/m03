// Consiste en un programa que simula una guardería de niños, donde se pueden añadir, listar y eliminar niños de la guardería.

                    ///solucion cambiar los string de %-15s %-10d  a por %s %d
                    ///System.out.printf("| %-15s | %-15s | %-10d |\n", niñes1.get(i).nom, niñes2.get(i).sexo, 
                    // linia 50 
                    ///System.out.print("Introduce la edad del niñe: ");

                    //ne.edat = sc.nextInt(); <--- revienta con string
                    /*  solucion 
                                  do{
                    edad=sc.nextLine();
                }while(!edad.matches("[0-9]+"));
                ne.edat = Integer.parseInt(edad); */

                /*  se cambiaron las lineas :System.out.printf("| %s | %s | %s |\n", "NOMBRE", "SEXO", "EDAD");System.out.printf("| %s | %s | %d |\n", niñes1.get(i).nom, niñes2.get(i).sexo,;
            
            } else if (op.equals("2")) {
                if (niñes1.isEmpty()) {
                    // Se verifica si no hay niños registrados
                    System.out.println("\nNo hay niñes registrados.");
                } else {
                    //
                    // Se muestra la lista de niños
                    System.out.println("\nLISTA DE AUTISMO:");
                    System.out.println("---------------------------------------------------");
                    System.out.printf("| %s | %s | %s |\n", "NOMBRE", "SEXO", "EDAD");
                    System.out.println("---------------------------------------------------");
                    

                    for (int i = 0; i < niñes1.size(); i++) {
                        // Se imprimen los datos de cada niño
                        System.out.printf("| %s | %s | %d |\n", niñes1.get(i).nom, niñes2.get(i).sexo,
                                niñes3.get(i).edat);
                    }
                    
                    System.out.println("---------------------------------------------------");
                     */
import java.util.ArrayList;
import java.util.Scanner;

public class Uf2Bug4  {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String op = "";
        ArrayList<NiñeN> niñes1 = new ArrayList<>();
        ArrayList<NiñeS> niñes2 = new ArrayList<>();
        ArrayList<NiñeE> niñes3 = new ArrayList<>();

        // Se imprime un mensaje de bienvenida
        System.out.println("GUARDERIA PETAQLOS");

        do {
            // Menú de opciones
            System.out.println("\n1. Introduce tu niñE de mierda.");
            System.out.println("2. Lista los mierdones de niñEs.");
            System.out.println("3. Echa a tu mierda de niñE de aquí");
            System.out.println("4. Salir de aquí!");
            System.out.print("Escoge la opción que quieres hacer niñE: ");
            op = sc.next();

            if (op.equals("1")) {
                // Se añade un niño a las listas
                NiñeN ni = new NiñeN();
                System.out.print("Introduce el nombre del niñe: ");
                ni.nom = sc.next();
                
                NiñeS ns = new NiñeS();
                System.out.print("Introduce el SEXO del niño (niño/niña/mucho): ");
                ns.sexo = sc.next(); // Lee la línea completa

                if (!ns.sexo.equals("niño") && !ns.sexo.equals("niña") && !ns.sexo.equals("mucho")) {
                    System.out.println("Opción no válida");
                    break;
                }
                
                niñes2.add(ns);
                NiñeE ne = new NiñeE();
                System.out.print("Introduce la edad del niñe: ");
                ne.edat = sc.nextInt();
                niñes3.add(ne);

                niñes1.add(ni);

            } else if (op.equals("2")) {
                if (niñes1.isEmpty()) {
                    // Se verifica si no hay niños registrados
                    System.out.println("\nNo hay niñes registrados.");
                } else {
                    // Se muestra la lista de niños
                    System.out.println("\nLISTA DE AUTISMO:");
                    System.out.println("---------------------------------------------------");
                    System.out.printf("| %-15i | %-15i | %-10s |\n", "NOMBRE", "SEXO", "EDAD");
                    System.out.println("---------------------------------------------------");
                    
                    for (int i = 0; i < niñes1.size(); i++) {
                        // Se imprimen los datos de cada niño
                        System.out.printf("| %-15s | %-15s | %-10d |\n", niñes1.get(i).nom, niñes2.get(i).sexo,
                                niñes3.get(i).edat);
                    }
                    
                    System.out.println("---------------------------------------------------");
                }

            } else if (op.equals("3")) {
                eliminarNiño(niñes1, niñes2, niñes3);

            } else if (op.equals("4")) {
                // Se imprime un mensaje de despedida y se sale del programa
                System.out.println("No vuelvas");
                break;

            } else {
                // Se maneja la opción inválida
                System.out.println("\nNo sabes ni poner un número del 1 al 4????");
            }
        } while (!op.equals("4"));
    }

    // Método para eliminar un niño de las listas
    public static void eliminarNiño(ArrayList<NiñeN> niñes1, ArrayList<NiñeS> niñes2, ArrayList<NiñeE> niñes3) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el nombre del niñe que deseas eliminar: ");
        String nombreEliminar = sc.next();

        for (int i = 0; i < niñes1.size(); i++) {
            if (niñes1.get(i).nom.equals(nombreEliminar)) {
                System.out.print("¿Estás seguro de que quieres eliminar a " + nombreEliminar + "? (s/n): ");
                String respuesta = sc.next();

                if (respuesta.equals("s")) {
                    // Se elimina al niño de las listas
                    niñes1.remove(i);
                    niñes2.remove(i);
                    niñes3.remove(i);
                    System.out.println(nombreEliminar + " ha sido eliminado de la guardería.");
                    return; // Termina el método después de eliminar al niño
                } else {
                    // Operación cancelada
                    System.out.println("Operación cancelada.");
                    return; // Termina el método si la operación es cancelada
                }
            }
        }

        // Si no se encuentra al niño
        System.out.println("No se encontró a " + nombreEliminar + " en la guardería.");
    }
}

class NiñeN {
    public String nom;
}

class NiñeS {
    public String sexo;
}

class NiñeE {
    public int edat;
}
