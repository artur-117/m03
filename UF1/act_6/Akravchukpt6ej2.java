package act_6;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt6ej2 {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Limpia la pantalla.
            System.out.print("\033[H\033[J");
            System.out.println("CONVERTIDOR DE KILOGRAMOS EN LOTS, FUNTS, PUDS Y BERKOVETS");
            // Guarda un valor double en kg escrito por teclado, se usara para los kilogramos.
            double kg = Double.parseDouble(buffer.readLine());
            // Comprueba si es positivo.
            if(0.0<kg){
                // Inicia las variables de unidades de peso.
                double kgtotal = kg;
                // Convierte kilogramos en gramos.
                double g = kg*1000.0;

                // Calcula los lots, luego los funts y resta la sobra de los lots.
                double lots=g/12.7974;
                int funts = (int) lots/32;
                lots -= funts*32;

                // Calcula los puds y resta los funds
                int puds = funts/40;
                funts -= puds*40;

                // Calcula los berkvoets y resta los puds
                int berkovets = puds/10;
                puds -= berkovets*10;
                
                // Muestra el resultado final.
                System.out.format("%.1f kg son %d berkovets %d puds %d funts %.1f lots.%n", kgtotal,berkovets,puds,funts,lots);
            }else{
                System.out.println("Por favor, escriba un número positivo.");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}