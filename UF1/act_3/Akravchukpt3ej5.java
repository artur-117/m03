public class Akravchukpt3ej5 {
    public static void main(String[] args) {
        // Asigna una variable int con un 0 inicial
        int n = 0;
        // Un bucle que va subando la variable que asignamos un 0 anteriormente a la i, la i va aumentando por uno cada vez que vuelve al bucle y termina sumando
        // todos los numeros de 0 al 100 a la n, lo que da como resultado 5050.
        for(int i = 0; i<101; n=n+(i++)){}
        // Muestra el resultado.
        System.out.println(n);
    }
}
