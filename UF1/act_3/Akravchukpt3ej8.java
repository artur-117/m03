import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt3ej8 {
    public static void main(String[] args) {
        // Inicia un try junto a BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Lee un numero y lo guarda en num.
            System.out.print("Dame un numero: ");
            int num = Integer.parseInt(buffer.readLine());
            System.out.println();
            // Muestra con un bucle for los divisores del numero dado anteriormente.
            System.out.print("Los divisores de "+num+" són: ");
            for(int i = 1; i<=num; i++){
                // Detecta si la i es divisor de num, si lo es, lo muestra por pantalla junto a un espacio permitiendo mostrar el siguiente.
                if(num%i==0){
                    System.out.print(i+" ");
                }
            }
            System.out.println();
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
