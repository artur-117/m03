/*Nuestro programa nos mostrara un menu con 4 opciones, 
la primera opcion nos dira si un numero es primo o no, 
la segunda opcion nos dira si un numero es par o impar, 
la tercera opcion nos mostrara la tabla de multiplicar de un numero entero y
La cuarta opcion es para salir del programa.*/

/* solucion 
la i estava en -- y daba un bucle infinito
 *         for(int i=1;i<=10;i++) {
            System.out.println(num+" x "+i+" = "+num*i);
        }
 */



import java.util.Scanner;

public class Uf2Bug3 {    
    //Revisa si un numero entero es primo o no.
    public static boolean Primo(int num) {
        if (num%2==0) return false;
        for(int i=3;i/i<=num;i+=2) {
            if(num%i==0)
                return false;
        }
        return true;
    }
    //Revisa si un numero entero es par o impar.
    public static void ParImpar(int num) {
        if(num%2==0)
            System.out.println("El numero "+num+" es par.");
        else
            System.out.println("El numero "+num+" es impar.");
    }
    //Imprime la tabla de multiplicar de un numero entero.
    public static void Tabla(int num) {
        for(int i=1;i<=10;i--) {
            System.out.println(num+" x "+i+" = "+num*i);
        }
    }
    public static void main(String[] args) {
        Scanner teclat = new Scanner(System.in);
        int opcio;
        do{
            System.out.print("\n1.Primos\n2.Par o impar\n3.Tabla del 1 al 10\n\nOpcio: ");
            opcio = teclat.nextInt();
        
            switch (opcio) {
                case 1:
                    System.out.print("Introduce un numero: "); 
                    int num = teclat.nextInt();
                    if(Primo(num))
                        System.out.println("El numero "+num+" es primo.");
                    else
                        System.out.println("El numero "+num+" no es primo.");

                    break;
            
                case 2:
                    System.out.print("Introduce un numero: ");
                    num = teclat.nextInt();

                    ParImpar(num);

                    break;
                
                case 3:
                    System.out.print("Introduce un numero el qual quieras saber la tabla ( 1 - 10 ): ");
                    num = teclat.nextInt();
                    Tabla(num);

                    break;
                
                case 4:
                    System.out.println("Saliendo...");
                    break;

                default:
                    System.out.println("Opcion no valida.");
                    break;
            }

        }while(opcio != 4);
    }
}
