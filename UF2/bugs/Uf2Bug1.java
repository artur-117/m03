/*
 * Este programa consiste en una Pizzeria muy básica. 
 * Has de crear tu propia pizza a tu gusto, con unos tipos de masa en específicos, los ingredientes tu mismo te los puedes inventar.
 * En este programa hay en menú creado para escoger opciones, la de crear la pizza, mostrar la pizzas almacenadas, eliminar alguna y la opción de salir del pograma.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

class Pizza {
    String ingredientes;
    String masa;
    String nombre;
}

public class Uf2Bug1 {
    public static boolean inicio = true;
    public static ArrayList<Pizza> pizza;
    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<Pizza> nuevaPizza = new ArrayList<Pizza>();

        while (inicio) {
            System.out.print("--- Creación de tu propia Pizza ---\n1. Crear pizza\n2. Mostrar pizzas almacenadas\n3. Eliminar pizza\n4. Salir\n- Escoge tu opción: ");   
            String opcion = bf.readLine();

            switch (opcion) {
                case "1":
                    Pizza pizza = new Pizza();
                    System.out.print("\n--- Empieza a crear tu pizza ---\n- ¿Cómo se va a llamar?: ");
                    pizza.nombre = bf.readLine();

                    boolean tipoMasa = true;
                    while (tipoMasa) {
                        System.out.print("- Escoge el tipo de masa (fina, normal o gruesa): ");
                        pizza.masa = bf.readLine();
                        // cambiado || por &&
                        if (!pizza.masa.equalsIgnoreCase("fina") && !pizza.masa.equalsIgnoreCase("normal") && !pizza.masa.equalsIgnoreCase("gruesa")){
                            System.out.println("\n¡Has de escoger un tipo de masa válido como los que se muestran en pantalla!\n");           
                        }
                        else {
                            tipoMasa = false;
                        }
                    }

                    System.out.print("- Añade el nombre de ingredientes: ");
                    pizza.ingredientes = bf.readLine();

                    nuevaPizza.add(pizza);

                    System.out.println("\n¡La pizza " + pizza.nombre + " ha sido creada con exito!\n");
                    break;
            
                case "2":
                    System.out.println("\n--- Pizzas almacenadas ---");
                    for (Pizza p : nuevaPizza) {
                        System.out.println("Nombre: " + p.masa);
                        System.out.println("Tipo de masa: " + p.masa);
                        System.out.println("Ingredientes: " + p.ingredientes + "\n");
                    }
                    break;

                case "3":
                    System.out.print("\n- Introduce el nombre de la pizza que desee tirar a la basura (Si quieres eliminar todas escribe *): ");
                    String eliminarPizza = bf.readLine();

                    if (eliminarPizza.equals("*")) {
                        System.out.println("\n- ¿Estás seguro que quieres eliminar todas las pizzas? (S para confirmar la acción)");
                        // cambiado eliminarPizzass a eliminarPizzas 
                        String eliminarPizzas = bf.readLine();

                        if (eliminarPizzas.equalsIgnoreCase("S")) {
                            for(int i = 0; i < nuevaPizza.size(); i++){
                                    nuevaPizza.remove(i);
                            }
                            System.out.println("\n¡Todas las pizzas han sido lanzadas a la basura con exito!\n");
                        }
                    }

                    for(int i = 0; i < nuevaPizza.size(); i++){
                        if (nuevaPizza.get(i).nombre.equalsIgnoreCase(eliminarPizza)) {
                            nuevaPizza.remove(i);
                            System.out.println("\n¡La pizza con el nombre: " + eliminarPizza + " ha sido lanzada a la basura!\n");
                        }
                    }
                    break;

                case "4":
                System.out.println("\n¡Buena suetre!\n");
                inicio = true;
                break;

                default:
                    System.out.println("\n¡Has introducido una opción incorreta!\n");
                    break;
            }
        }
    }
}
