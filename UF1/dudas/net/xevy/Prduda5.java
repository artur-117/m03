import java.util.Scanner;

public class Prduda5 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try{        
            // Comprueba si el numero entero obtenido es igual a 1000, si lo es ganaste un premio, si no, no lo ganaste.
            System.out.println(geti()==1000?"¡Ganaste un premio!":"¡No ganaste nada!");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static int geti(){
		// Obtiene un numero entero.
        int i = scanner.nextInt();
        return i;
    }
}
