package examen_3;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Inicia el boolean menu, cuando sea falso dejara de salir el menu.
            Boolean menu = true;
            // Inicia una lista para los enfermos.
            ArrayList<Enfermos> enfermos = new ArrayList<Enfermos>();
            while(menu){
                // Limpia la pantalla y muestra el menu por pantalla..
                System.out.print("\033[H\033[J");
                System.out.println("1. Ingresar pacientes.");
                System.out.println("2. Consultar datos de un paciente por número de habitación.");
                System.out.println("3. Consultar datos de un paciente por nombre.");
                System.out.println("4. Listar a todos los pacientes.");
                System.out.println("5. Dar de baja a un paciente.");
                System.out.println("6. Salir");
                int opcion = Integer.parseInt(buffer.readLine());
                // Switch con las distintas opciones, entrara a un case dependiendo de la opción seleccionada.
                switch(opcion){
                    case 1:
                        // Limita en 50.
                        if(enfermos.size()<50){
                            // Crea un nuevo enfermo y deja escribir guardando sus valores en una lista.
                            Enfermos enfermo = new Enfermos();
                            System.out.print("Nombre: ");
                            enfermo.nombre = buffer.readLine();
                            System.out.print("Fecha nacimiento: ");
                            enfermo.fecha_nacimiento = buffer.readLine();
                            System.out.print("Habitacion: ");
                            enfermo.habitación = Integer.parseInt(buffer.readLine());
                            enfermos.add(enfermo);
                            break;
                        }else{
                            // Si ya hay más de 50 pacientes, no se puede añadir más.
                            System.out.println("Maxima cantidad de pacientes superada.");
                            System.out.print("Enter para continuar.");
                            buffer.readLine();
                            break;
                        }
                    case 2:
                        // Pide la habitacion para comprobar por habitacion.
                        System.out.print("Habitacion: ");
                        int habitacion_sel = Integer.parseInt(buffer.readLine());
                        // Bucle que busca por todas las habitaciones.
                        for(int i = 0; i<enfermos.size(); i++){
                            if(enfermos.get(i).habitación == habitacion_sel){ 
                                System.out.println(enfermos.get(i).nombre+" "+enfermos.get(i).fecha_nacimiento+" "+enfermos.get(i).habitación);
                            }
                        }
                        System.out.print("Enter para continuar.");
                        buffer.readLine();
                        break;
                    case 3:
                        // Pide el nombre y por un bucle lo buscas.
                        System.out.print("Nombre: ");
                        String nombre_sel = buffer.readLine();
                        for(int i = 0; i<enfermos.size(); i++){
                            if(enfermos.get(i).nombre.equals(nombre_sel)){ 
                                System.out.println(enfermos.get(i).nombre+" "+enfermos.get(i).fecha_nacimiento+" "+enfermos.get(i).habitación);
                            }else{
                                System.out.println("No hay ningún paciente con este nombre.");
                            }
                        }
                        System.out.print("Enter para continuar.");
                        buffer.readLine();
                        break;
                    case 4:
                        // Muestra con formato todos los pacientes usando un foreach para ir por toda la lista.
                        System.out.println("Nombre  Fecha Nacimiento    Habitación");
                        System.out.println("======================================");
                        for (Enfermos enfermos2 : enfermos) {
                            System.out.println(enfermos2.nombre+"   "+enfermos2.fecha_nacimiento+"          "+enfermos2.habitación);
                        }
                        buffer.readLine();
                        break;

                    case 5:
                        // Limpia la pantalla y muestra el menu de borrado por pantalla..
                        System.out.print("\033[H\033[J");
                        System.out.println("¡Cuidado!");
                        System.out.println("¡Estas a punto de borrar los datos de un paciente!");
                        System.out.println("Si estás seguro de querer dar de baja a pacientes, elige con que metodo deseas continuar:");
                        System.out.println("1- Nombre");
                        System.out.println("2- Fecha nacimiento");
                        System.out.println("3- Habitación");
                        System.out.println("4- Cancelar");
                        int metodoborrado = Integer.parseInt(buffer.readLine());
                        // Switch que selecciona para seleccionar el metodo de borrar, todos funcionan similar, piden el valor y luego buscan por ese valor para borrarlo.
                        switch(metodoborrado){
                            case 1:
                                System.out.print("Nombre: ");
                                nombre_sel = buffer.readLine();
                                for(int i = 0; i<enfermos.size(); i++){
                                    if(enfermos.get(i).nombre.equals(nombre_sel)){ 
                                        System.out.println(enfermos.get(i).nombre+" "+enfermos.get(i).fecha_nacimiento+" "+enfermos.get(i).habitación);
                                        System.out.println("Confirma que quieres dar de baja a este paciente, escribe ''CONFIRMO''.");
                                        String confirmacion = buffer.readLine();
                                        if(confirmacion.equals("CONFIRMO")){
                                            enfermos.remove(i);
                                        }
                                        confirmacion = " ";
                                    }
                                }
                                break;
                            case 2:
                                System.out.print("Fecha nacimiento: ");
                                String fecha_sel = buffer.readLine();
                                for(int i = 0; i<enfermos.size(); i++){
                                    if(enfermos.get(i).fecha_nacimiento.equals(fecha_sel)){ 
                                        System.out.println(enfermos.get(i).nombre+" "+enfermos.get(i).fecha_nacimiento+" "+enfermos.get(i).habitación);
                                        System.out.println("Confirma que quieres dar de baja a este paciente, escribe ''CONFIRMO''.");
                                        String confirmacion = buffer.readLine();
                                        if(confirmacion.equals("CONFIRMO")){
                                            enfermos.remove(i);
                                        }
                                    }
                                }
                                break;
                            case 3:
                                System.out.print("Habitación: ");
                                habitacion_sel = Integer.parseInt(buffer.readLine());
                                for(int i = 0; i<enfermos.size(); i++){
                                    if(enfermos.get(i).habitación == habitacion_sel){ 
                                        System.out.println(enfermos.get(i).nombre+" "+enfermos.get(i).fecha_nacimiento+" "+enfermos.get(i).habitación);
                                        System.out.println("Confirma que quieres dar de baja a este paciente, escribe ''CONFIRMO''.");
                                        String confirmacion = buffer.readLine();
                                        if(confirmacion.equals("CONFIRMO")){
                                            enfermos.remove(i);
                                        }
                                    }
                                }

                                break;
                        }
                        System.out.println("Enter para continuar.");
                        buffer.readLine();
                        break;

                    case 6:
                        // desactiva menu
                        menu = false;
                        break;
                    default:
                        // fallo menu
                        System.out.println("Hubo un fallo al seleccionar la opción...");
                        System.out.println("Pulsa ENTER para volver a intentarlo, ¡Si quieres salir selecciona 6 en el menu!");
                        buffer.readLine();
                        break;

                }     
            }
        }catch(Exception e){
            // si llega aqui hubo fallo.
            e.printStackTrace();
        }
    }
}
