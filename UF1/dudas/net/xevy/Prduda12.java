import java.util.Scanner;

public class Prduda12 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
            // Guarda el escrito en la variable letra.
            String letra = scanner.nextLine();
            // Comprueba si solo es una letra.
            if(letra.length()==1){
                //Baja a minusculas.
                String letrac = letra.toLowerCase();
                //Comprueba si es vocal, si lo es dice que es vocal y si no es dice que no es vocal.
                System.out.println(letrac.equals("a")||letrac.equals("e")||letrac.equals("i")||letrac.equals("o")||letrac.equals("u")?"es vocal":"no es vocal");
            }else{
                // Si no es una sola letra no lo procesa.
                System.out.println("No se puede procesar el dato.");
            }
    }

}
