package act_6;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukpt6ej5 {
    public static void main(String[] args) {
        // Limpia la pantalla.
        System.out.print("\033[H\033[J");

        System.out.println("CRUCES SEGUIDAS");
        System.out.println("¿Cuántas veces se va a tirar la moneda?");
        // Inicia un try junto a un bufferedreader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){

            int veces = Integer.parseInt(buffer.readLine());
            if(2<=veces){
                // Inicia una variable aleatoria.
                Random aleatorio = new Random();
                // Inicia las variables que comprobaran las cruces anteriores, las cruces seguidas, el maximo de cruces que hubieron y si han habido cruces.
                int tiroanterior = 0;
                int crucesseguidas = 0;
                int crucesmax = 0;
                Boolean haycruz = false;
                // Tira las monedas la cantidad de veces que se pidio.
                for(int i=0;i<veces;i++){
                    // Saca el resultado de la moneda.
                    int tiro = aleatorio.nextInt(2);
                    // Si es 0, es cara.
                    if(tiro==0){
                        System.out.print("\u26AA");
                    }else{
                        // Si no es 0, es cruz.
                        haycruz = true;
                        System.out.print("\u2716");
                        // Si el tiro anterior también fue cruz, han salido seguidas.
                        if(tiroanterior == 1){
                            // Si es la primera vez que sale cruz seguida, se suman 2.
                            if(crucesseguidas==0){
                                crucesseguidas=crucesseguidas+2;
                            }else{
                                // Si no, solo se suma 1.
                                crucesseguidas++;
                            }
                            // Si cruces seguidas es mayor al maximo de cruces, se le aumenta el crucesmax a crucesseguidas.
                            if(crucesmax<crucesseguidas){
                                crucesmax = crucesseguidas;
                            }
                        }else{
                            // Si la anterior no fue cruz, no hay seguidas.
                            crucesseguidas=0;
                        }
                    }
                    // Registra el anterior tiro con el actuar para la siguiente tirada.
                    tiroanterior = tiro;
                }
                System.out.println();
                // Si hubo cruz, dara estos mensajes.
                if(haycruz){
                    // Si hubo seguidas.
                    if(1<crucesmax){
                        System.out.println("Se han llegado a obtener "+crucesmax+" cruces seguidas.");
                    }else{
                        // Si no tuvo seguidas.
                        System.out.println("Ha salido alguna cruz, pero no ha llegado a salir dos cruces seguidas.");
                    }
                }else{
                    // Si no salio ninguna cruz.
                    System.out.println("No ha salido ninguna cruz.");
                }
                System.out.println();
            }else{
                System.out.println("¡La moneda se debe tirar al menos dos veces!");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
