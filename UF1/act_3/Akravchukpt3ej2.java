// El Akravchukpt3ej2b.java es el que funciona correctamente, este tiene un fallo.

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Akravchukpt3ej2 {
    public static void main(String[] args) {
        // Inicia un try junto al BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Iniciamos num.
            int num=1;

            // Bucle que se ejecuta mientras numero no sea multiplo de 10.
            while(num%10!=0){
                // Limpia la pantalla.
                System.out.print("\033[H\033[J");  
                
                // Te pide seleccionar el num.
                System.out.print("Escribe un numero multiplo de 10: ");
                num = Integer.parseInt(buffer.readLine());
                int num2=num;
                int i = 0;

                // Si 100 es menor a num2.
                if(100<num2){
                    // Y mientras 100 sea menor a 2, divide por 10 y va guardando la i como longitud.
                    while(100<num2){
                        i=(num2<100)?i:i+1;
                        num2 = num2/10;
                    }
                }else{
                    // Si no, lo divide una vez por 10.
                    num2 = num2/10;
                }
                // Muestra el resultado.
                System.out.println(num2+" por 10 elevado a "+i);
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}
