import java.util.Scanner;

public class Pr14 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try{
            //Obtiene el numero escrito por teclado y te dice si es bisiesto o no.
            if(geti()%4==0){
                System.out.println("Es bisiesto.");
            }else{
                System.out.println("No es bisiesto.");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static int geti(){
        // Lee el numero entero que se envie por teclado.
        int i = scanner.nextInt();
        return i;
    }
}


