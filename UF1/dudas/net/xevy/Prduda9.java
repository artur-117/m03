import java.util.Scanner;

public class Prduda9 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
            // Guarda en num el numero entero obtenido.
            int num = scanner.nextInt();
            //Si es negativo, multiplica por -1, si no, devuelve el numero por pantalla.
            System.out.println(num<0?num*-1:num);
    }

    


}
