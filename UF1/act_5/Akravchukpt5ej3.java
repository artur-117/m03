package act_5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;


public class Akravchukpt5ej3 {
    public static BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) {
        try{
            // Inicia el bollean nc como true, lo usaremos para comprobar si el nombre es correcto o no para las condiciones.
            Boolean nc = true;
            String nombre = buffer.readLine();
            // Establece la longitud del nombre entre 6 y 12 caracteres.
            if(6<=nombre.length()&&nombre.length()<=12){
                // Va por cada caracter comprobando si el nombre cumple con los caracteres de 0-9,a-z y A-Z, si lo cumple, no pasa nada.
                for(int i = 0; i<nombre.length();i++){
                    if(nombre.charAt(i) >= '0' & nombre.charAt(i)<='9'||nombre.charAt(i)>='a' && nombre.charAt(i)<='z'||nombre.charAt(i)>='A' && nombre.charAt(i)<='Z'){
                    }else{
                        // Si no lo cumple, el nc se pone en falso y te devuelve que debe contener solo letras y numeros.
                        nc = false;
                        System.out.println("El nombre debe contener solo letras y numeros.");
                        break;
                    }
                }
                // Si llegado a este punto nc sigue siendo verdadero, devuelve el nombre por pantalla dando un true.
                if(nc){
                    JOptionPane.showMessageDialog(null, "Tu nombre es "+nombre, nombre, JOptionPane.INFORMATION_MESSAGE);
                    System.out.println(true);
                }else{
                    // Si no, devuelve falso.
                    System.out.println(false);
                }
            }else{
                if(nombre.length()<6){
                    // Aclara que debe tener minimo 6 caracteres.
                    System.out.println("El nombre debe contener almenos 6 caracteres.");
                }else{
                    // Aclara que debe tener maximo 12 caracteres.
                    System.out.println("El nombre debe contener como maximo 12 caracteres.");
                }
                System.out.println(false);
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}