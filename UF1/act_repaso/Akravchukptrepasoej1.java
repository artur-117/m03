package act_repaso;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

public class Akravchukptrepasoej1 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            Random random = new Random();
            int a = random.nextInt(9)+2;
            int b = random.nextInt(9)+2;
            System.out.println("¿Cuanto es "+a+" x "+b+"?");
            System.out.println((Integer.parseInt(buffer.readLine())==a*b)?"¡RESPUESTA CORRECTA!":"¡RESPUESTA INCORRECTA!");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
