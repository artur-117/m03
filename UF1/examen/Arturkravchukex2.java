import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Arturkravchukex2 {
    public static void main(String[] args) {
        System.out.println("VELOCIDAD");
        System.out.print("Unidad de distancia (km o m):");

        // Inicia un try junto al BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Guarda un valor String en opciond escrito por teclado para usarlo como la opcion de Km o M (kilometro o metro).
            String opciond = buffer.readLine();
            if(opciond.equals("KM")||opciond.equals("km")||opciond.equals("M")||opciond.equals("m")){
                System.out.print("Distancia recorrida:");
                // Guarda un valor double para la distancia.
                double d = Double.parseDouble(buffer.readLine());
                System.out.print("Unidad de tiempo (h o s)");
                // Guarda un valor String en opciont escrito por teclado para usarlo como la opcion de H o S (hora o segundo).
                String opciont = buffer.readLine();
                if(opciont.equals("H")||opciont.equals("h")||opciont.equals("M")||opciont.equals("m")){
                    System.out.print("Tiempo empleado:");
                    // Guarda un valor double para el tiempo.
                    double t  = Double.parseDouble(buffer.readLine());
                    // Inicia ts y res para usarlos más adelante.
                    double ts = 0;
                    double res = 0;
                    // Un switch que selecciona entre Km o M.
                    switch(opciond){
                        // Selecciona kilometros.
                        case "KM":
                        case "km":
                            // Pasa km a metros.
                            double m = d*1000.0;
                            // Un switch que selecciona entre h y s.
                            switch(opciont){
                                // Selecciona horas.
                                case "H":
                                case "h":
                                    // Pasa horas a segundos.
                                    ts = t*(3600.0/1.0);
                                    // Resuelve la velocidad.
                                    res = m/ts;
                                    System.out.println("Si ha recorrido "+d+" km en "+t+"h su velocidad ha sido "+res+"m/s ("+res*3.6+"km/h).");
                                    break;
                                // Selecciona segundos.
                                case "S":
                                case "s":
                                    // Resuelve la velocidad.
                                    res = m/t;
                                    System.out.println("Si ha recorrido "+d+" km en "+t+"s su velocidad ha sido "+res+"m/s ("+res*3.6+"km/h).");
                                    break;
                                default:
                                    System.out.println("La unidad de tiempo que ha indicado no es s o h.");
                                    break;
                            }
                            break;
                        // Selecciona metros.
                        case "M":
                        case "m":
                            // Un switch que selecciona entre h y s.
                            switch(opciont){
                                // Selecciona horas.
                                case "H":
                                case "h":
                                    // Pasa horas a segundos.
                                    ts = t*(3600.0/1.0);
                                    // Resuelve la velocidad.
                                    res = d/ts;
                                    System.out.println("Si ha recorrido "+d+" m en "+t+"h su velocidad ha sido "+res+"m/s ("+res*3.6+"km/h).");
                                    break;

                                // Selecciona segundos.
                                case "S":
                                case "s":
                                    // Resuelve la velocidad.
                                    res = d/t;
                                    System.out.println("Si ha recorrido "+d+" m en "+t+"s su velocidad ha sido "+res+"m/s ("+res*3.6+"km/h).");
                                    break;
                                default:
                                    System.out.println("La unidad de tiempo que ha indicado no es s o h.");
                                    break;
                            }
                            break;
                        default:
                            System.out.println("La unidad de distancia que ha indicado no es m o km.");
                            break;
        
                    }
                // Si la unidad dada no es s o h, sale del if y muestra lo siguiente por pantalla.
                }else{
                System.out.println("La unidad de tiempo que ha indicado no es s o h.");
                }
            // Si la unidad dada no es km o m, sale del if y muestra lo siguiente por pantalla.
            }else{
                System.out.println("La unidad de distancia que ha indicado no es m o km.");
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        } 

    }
}
