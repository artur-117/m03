package act_4;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt4ej1 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Lee la fecha y inicia suerte.
            int fecha = Integer.parseInt(buffer.readLine());
            int suerte = 0;
            // 2 bucles para dividir la fecha y luego sumarla entre todo, primero lo hace entre la fecha y luego entre los dos numeros restantes si los hay, finalmente muestra el resultado.
            for(int i = 0; i<8;suerte = suerte+(fecha%10),fecha=fecha/10,i++);
            suerte = (10<suerte)?(suerte%10)+(suerte/10):suerte;
            System.out.println(suerte);
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}