import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Akravchukpt3ej7 {
    public static void main(String[] args) {
        // Inicia un try con BufferredReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            // Lee el ancho y la altura del rectangulo.
            System.out.print("Ancho del rectangulo: ");
            int ancho = Integer.parseInt(buffer.readLine());
            System.out.println();
            System.out.print("Altura del rectangulo: ");
            int altura = Integer.parseInt(buffer.readLine());
            // Bucle que muestra el ancho en *.
            for(int i = 0; i<ancho; i++){
                System.out.print("*");
            }
            System.out.println();
            // Bucle que muestra la altura en *.
            for(int i = 0; i<altura-2; i++){
                System.out.println("*"+" ".repeat(ancho-2)+"*");
            }
            // Bucle que muestra el ancho en *.
            for(int i = 0; i<ancho; i++){
                System.out.print("*");
            }
            System.out.println();
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }    
}