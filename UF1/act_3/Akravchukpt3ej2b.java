import java.io.BufferedReader;
import java.io.InputStreamReader;


public class Akravchukpt3ej2b {
    public static void main(String[] args) {
        // Inicia un try junto al BufferedReader.
        try(BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in))){
            int num=1;
            while(num%10!=0){
                // Limpia la palabra.
                System.out.print("\033[H\033[J"); 
                // Lee un numero multiplo de 10, si no es multiplo se repetira hasta ser multiplo limpiando la pantalla.
                System.out.print("Escribe un numero multiplo de 10: ");
                num = Integer.parseInt(buffer.readLine());
                // Mete el numero a un string en array separando todos los digitos.
                String[] numstr = Integer.toString(num).split("");
                // Detecta la longitud de la array.
                int l = numstr.length;
                // El numero final sera el mismo que numero.
                int numfinal = num;
                // La longitud inicial estara en 0.
                int longitud = 0;
                // Bucle que saca primero la longitud y luego hace las divisiones necesarias entre 10 para sacar los numeros correctos.
                for(int i = l-1; i!=0; i--){
                    if(Integer.parseInt(numstr[i])==0){
                        longitud++;
                    }else{
                        for(int j = 0; j<longitud; j++){
                            numfinal=numfinal/10;
                        }
                        break;
                    }
                }
                // Muestra el resultado.
                System.out.println(numfinal+" por 10 elevado a "+longitud);
            }
        }catch(Exception e){
            // Si llega aquí, significa que hubo un fallo.
            e.printStackTrace();
        }
    }
}