import java.util.Scanner;

public class Pr11 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        try{
            //Obtiene el numero escrito por teclado y te dice si es par o no.
            if(geti()%2==0){
                System.out.println("Es par.");
            }else{
                System.out.println("No es par.");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }  

    public static int geti(){
        // Lee el numero entero que se envie por teclado.
        int i = scanner.nextInt();
        return i;
    }
}
