package act_5;

import java.util.Arrays;
import java.util.List;

public class Akravchukpt5ej6 {
    public static void main(String[] args) {
        // Inicia una variable de lista guardando valores de la siguiente forma: [1,[2,[3,4]],5]
        List<Object> n = Arrays.asList(1,Arrays.asList(2,Arrays.asList(3,4)),5);
        // Muestra la lista por pantalla.
        System.out.println(n);
        // Guarda el primer index de la lista anterior en la variable i.
        String i = n.get(1).toString();
        // Modifica la lista dejandola de la siguiente manera: [1,[2,[3,4],[6,7]],5]
        n.set(1, Arrays.asList(i.substring(1,2),Arrays.asList(i.substring(5, 9)),Arrays.asList(6,7)));
        // Muestra por pantalla el resultado final.        
        System.out.println(n);
    }
}